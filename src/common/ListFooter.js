import React, { Component } from "react";
import {View,ActivityIndicator} from "react-native";
import * as colors from '../assets/colors'
import { moderateScale} from '../utils/responsiveDimensions';

export default class ListFooter extends Component {

    render() {
        return (
        <View>
            <ActivityIndicator style={{marginVertical:moderateScale(6)}} color={colors.darkBlue} />
        </View>
        );
    }
}
