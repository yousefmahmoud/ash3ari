//
import React, { Component } from "react";
import { Text, View, ActivityIndicator, TouchableOpacity, TouchableWithoutFeedback, ScrollView } from "react-native";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from "react-redux";
import Strings from '../assets/strings';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { arrabicFont, englishFont } from '../common/AppFont'
import { Icon } from 'native-base'


class AppSuccessDialog extends Component {

    render() {
        const { isRTL, open, message, onPress } = this.props
        //opne: use it to show/hide bottom sheet (require)
        //onOutPress : action to close bottom sheet when press out of it (require)
        return (
            open ?
                <TouchableOpacity
                    activeOpacity={1} style={{ zIndex: 10000000, elevation: 10, position: 'absolute', left: 0, bottom: 0, right: 0, top: 0, backgroundColor: 'rgba(0,0,0,0.4)', alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}
                >

                    <TouchableOpacity activeOpacity={1}>
                        <Animatable.View useNativeDriver duration={500} animation='slideInDown'>
                            <View style={{ alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(30), backgroundColor: 'white', borderRadius: moderateScale(3) }} >
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 80, borderRadius: 40, backgroundColor: 'white', marginTop: moderateScale(-18) }} >
                                    <Icon name='check' type='AntDesign' style={{ fontSize: responsiveFontSize(20), color: colors.darkBlue }} />
                                </View>

                                <Text style={{ fontSize: responsiveFontSize(8), color: colors.darkBlue, marginTop: moderateScale(5), fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.success}</Text>

                                <Text style={{ fontSize: responsiveFontSize(6), color: "black", marginTop: moderateScale(5), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(80), textAlign: 'center' }} >{message}</Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        if (onPress) {
                                            onPress()
                                        }
                                    }}
                                    style={{ justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(10), width: responsiveWidth(70), borderRadius: moderateScale(2), height: responsiveHeight(7), backgroundColor: colors.darkBlue }} >
                                    <Text style={{ fontSize: responsiveFontSize(7), color: 'white', fontFamily: isRTL ? arrabicFont : englishFont }} > {Strings.ok} </Text>
                                </TouchableOpacity>
                            </View>
                        </Animatable.View>
                    </TouchableOpacity>
                </TouchableOpacity>
                : null

        )
    }
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(AppSuccessDialog);
