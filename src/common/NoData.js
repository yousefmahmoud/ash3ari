//
import React, { Component } from "react";
import { Text, View,Image} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import strings from '../assets/strings'
import {englishFont} from '../common/AppFont'
import FastImage from "react-native-fast-image";

class NoData extends Component {
render(){
    return(
        <View style={{marginTop:moderateScale(20), backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
            {/*<LottieView
            style={{width:responsiveWidth(50)}}
            source={require('../assets/animations/noData.json')}
            autoPlay
            loop
            />*/}
            <FastImage
              style={{width:responsiveWidth(80),height:responsiveWidth(80)}}
              source={require('../assets/imgs/noData.png')}
            />
            {/*<Text style={{marginTop:moderateScale(-10), fontFamily:englishFont, fontSize:responsiveFontSize(7)}} >{strings.NoData}</Text>*/}
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(NoData);
