

import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, Linking, TouchableOpacity, Text, Modal, ImageBackground, TextInput, Image
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import { Rating, AirbnbRating } from 'react-native-ratings';
import MapView, { Marker } from 'react-native-maps';
import HTML from 'react-native-render-html';
import WebView from 'react-native-webview'
import FastImage from 'react-native-fast-image'


class ServiceProviderPage extends Component {

    state = {
        works: [],
        showModal: '',
        work: {},
    }




    showDialogRegisterFirst = () => {
        const { isRTL, message, buttonTitle } = this.props
        return (
            <Dialog
                width={responsiveWidth(90)}
                onHardwareBackPress={() => { this.setState({ showSentOrderDialog: false }) }}
                visible={this.state.showSentOrderDialog}
            /*onTouchOutside={() => {
                this.setState({ showSentOrderDialog: false });
            }}*/
            >
                <View style={{ width: responsiveWidth(90), marginVertical: moderateScale(10), alignItems: 'center' }}>
                    <Text style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{message}</Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ showSentOrderDialog: false })
                            pop()
                        }}
                        style={{ width: responsiveWidth(30), height: responsiveHeight(6), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(2), marginTop: moderateScale(3) }}>

                        <Text style={{ color: 'white', fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{buttonTitle}</Text>
                    </TouchableOpacity>




                </View>
            </Dialog>
        )
    }




    render() {
        const { currentUser, isRTL } = this.props;
        // const { name, email, phone, message } = this.state
        return (
            this.showDialogRegisterFirst()
        );
    }

}

const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(ServiceProviderPage);