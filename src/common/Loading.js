//
import React, { Component } from "react";
import { Text, View, ActivityIndicator, Dimensions } from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";


class Loading extends Component {

    render() {
        return (
           /* <View style={{ width: responsiveWidth(25), marginTop: moderateScale(20), backgroundColor: 'tranparent', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                <LottieView
                    style={{ width: responsiveWidth(50) }}
                    source={require('../assets/animations/bigSpinner.json')}
                    autoPlay
                    loop
                />
                /  <Text style={{marginTop:moderateScale(5), fontSize:responsiveFontSize(7),fontWeight:'700'}} >LOADING</Text> /
        </View>*/

<View style={{ position: 'absolute', left: Dimensions.get('window').width / 2 - 30, top: Dimensions.get('window').height / 2 - 160, width: 60, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 8, zIndex: 9999 }}>
<ActivityIndicator size="large" color='black' />
</View>
        )
    }

}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(Loading);
