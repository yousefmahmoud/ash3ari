import React, { Component } from 'react';
import {
  View, TouchableOpacity, Text, ImageBackground, Animated
} from 'react-native';
import { Icon, Button, Badge } from 'native-base';
import { connect } from 'react-redux';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { openSideMenu, push, pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from './AppFont'
import FastImage from 'react-native-fast-image'
import * as colors from '../assets/colors'


class AppHeader extends Component {
  //openSideMenu(true,isRTL)


  render() {
    const { isRTL, title, back } = this.props;
    return (
      <View style={{ backgroundColor: colors.darkBlue, width: responsiveWidth(100), height: responsiveHeight(35) }}>
        <View style={{ marginTop: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between', width: responsiveWidth(98), alignSelf: 'center' }}>

          {back ?
            <Button
              onPress={() => { pop() }}
              style={{ backgroundColor: colors.darkBlue, elevation: 0, shadowOffset: { height: 0, width: 0 } }}
            >
              <Icon name={isRTL ? 'arrowright' : 'arrowleft'} type='AntDesign' style={{ color: colors.white }} />
            </Button>
            :
            <View style={{ width: responsiveWidth(10) }} />
          }

          <FastImage
            style={{ height: responsiveHeight(20), width: responsiveWidth(25) }}
            source={require('../assets/imgs/appLogo.png')}
          />

          <Button
            onPress={() => { openSideMenu(true, isRTL) }}
            style={{ backgroundColor: colors.darkBlue, elevation: 0, shadowOffset: { height: 0, width: 0 } }} >
            <Icon name='md-menu' type='Ionicons' style={{ color: colors.white }} />
          </Button>




        </View>
        {title &&
          <Text style={{ fontSize: responsiveFontSize(7), alignSelf: 'center', marginTop: moderateScale(5), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.white }}>{title} </Text>
        }
      </View>
    );
  }
}


const mapStateToProps = state => ({
  isRTL: state.lang.RTL,
  color: state.lang.color,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps)(AppHeader);
