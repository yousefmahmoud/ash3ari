//
import React, { Component } from "react";
import { Text, View} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import {Icon} from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import {arrabicFont,englishFont} from './AppFont'

class NotificationMessageError extends Component {
render(){
    const {isRTL,type,text} = this.props
    return(
        type=='error'?
        <View style={{width:responsiveWidth(100),height:responsiveHeight(12), flexDirection:isRTL?'row-reverse':'row', backgroundColor:'white',alignItems:'center'}}>
           <View style={{width:30,height:30,borderRadius:15,justifyContent:'center',alignItems:'center',backgroundColor:'black', marginHorizontal:moderateScale(7)}} >
                <Icon name='close' type='AntDesign' style={{fontSize:responsiveFontSize(7), color:'red'}} />
            </View>
            <Text style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(7),fontWeight:'700'}} >{text}</Text>
        </View>
        :
        <View style={{width:responsiveWidth(100),height:responsiveHeight(12), flexDirection:isRTL?'row-reverse':'row', backgroundColor:'white',alignItems:'center'}}>
        <   View style={{width:30,height:30,borderRadius:15,justifyContent:'center',alignItems:'center',backgroundColor:'green', marginHorizontal:moderateScale(7)}} >
                <Icon name='check' type='AntDesign' style={{fontSize:responsiveFontSize(7), color:'white'}} />
            </View>
         <Text style={{color:'gray', fontFamily:isRTL?arrabicFont:englishFont, fontSize:responsiveFontSize(7),fontWeight:'700'}} >{text}</Text>
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    //errorText: state.auth.errorText
});

export default connect(mapStateToProps)(NotificationMessageError);
