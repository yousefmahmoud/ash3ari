import React,{Component} from 'react';
import {View,RefreshControl,FlatList} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';
import LottieView from 'lottie-react-native';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../common/ListFooter';
import VisitCard from '../components/VisitCard';
import {removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import {enableSideMenu,push} from '../controlls/NavigationControll'
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import NetInfo from "@react-native-community/netinfo";


class Visits extends Component {

    page=1;
    state= {
        networkError:null,
        visits:[],
        refresh:false,
        loading:true,
        pages:null,
    }

   
       
    componentWillUnmount(){
    this.props.removeItem()
    }

   

    componentDidUpdate(){
    enableSideMenu(true,this.props.isRTL)
    }

    componentDidMount(){
        enableSideMenu(true,this.props.isRTL)
        NetInfo.addEventListener(state => {
            if(state.isConnected){
                this.page=1
                this.getVisits(1,false);
                this.setState({loading:true,networkError:false})
            }else{
                this.setState({loading:false,networkError:true})
            }
          });    
    }



    getVisits(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}visit/list?page=${page}`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false})
            }
            axios.post(uri,JSON.stringify({
                user_id:this.props.currentUser.user.id,
                token:this.props.currentUser.token
            }),{
                headers: {
                  'Content-Type': 'application/json',
                },
            })
                .then(response => {
                   
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:1,
                        networkError:null,
                        visits:refresh?response.data.patient_visits:[...this.state.visits,...response.data.patient_visits] ,
                    })
                }).catch(error => {
                    this.setState({networkError:true, loading:false,refresh:false})
                    
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }



    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader menu  title={Strings.visits}  /> 
                
                <View style={{ flex:1,justifyContent:'center',alignItems:'center'}}> 
                {this.state.loading?
                 <Loading />
                :
                this.state.networkError?
                <NetworError />
                :
                this.state.visits.length>0?
                <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.visits}
                renderItem={({item})=><VisitCard data={item} />}
                ListFooterComponent={this.renderFooter}
                refreshControl={
                <RefreshControl colors={["#B7ED03"]} refreshing={this.state.refresh}
                onRefresh={() => {
                  this.page = 1
                  this.getVisits(this.page, true);
                }} 
                />
                
                }
                /*onEndReached={() => {
                    if(this.page <= this.state.pages){
                        this.page++;
                        this.getVisits(this.page, false);
                    }
                    
                }}*/
                onEndReachedThreshold={.5}
                />
                :
                <NoData/>
            }
               
               </View>
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Visits);
