import React, { Component } from 'react';
import { View, RefreshControl, Alert, FlatList, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import ListFooter from '../common/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import { enableSideMenu, pop } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header'
import PageTitleLine from '../components/PageTitleLine'
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class Wallet extends Component {

    page = 1;
    list = [];
    state = {
        networkError: null,
        rooms: [],
        refresh: false,
        loading: true,
        pages: null,
        paymentType: 0,

    }



    componentDidMount() {
        enableSideMenu(true, null)
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { paymentType } = this.state
        return (
            <View>

                <PageTitleLine title={Strings.wallet} iconName='user-alt' iconType='FontAwesome5' />

                <View style={{ marginTop: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>
                    <Text style={{ fontSize: responsiveFontSize(8), marginHorizontal: moderateScale(7), color: '#6bb4cf', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.paymentOptions}</Text>
                </View>



                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(90), alignSelf: 'center', justifyContent: 'space-between', marginVertical: moderateScale(2), marginTop: moderateScale(10) }}>
                    <View style={{ width: responsiveWidth(58), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }} >
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.outstandingBalance}</Text>
                    </View>

                    <View style={{ width: responsiveWidth(28), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }}>
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>150 {Strings.RS}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(90), alignSelf: 'center', justifyContent: 'space-between', marginVertical: moderateScale(2) }}>
                    <View style={{ width: responsiveWidth(58), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }} >
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.paid}</Text>
                    </View>

                    <View style={{ width: responsiveWidth(28), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }}>
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>120 {Strings.RS}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(90), alignSelf: 'center', justifyContent: 'space-between', marginVertical: moderateScale(2) }}>
                    <View style={{ width: responsiveWidth(58), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }} >
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.remaining}</Text>
                    </View>

                    <View style={{ width: responsiveWidth(28), paddingVertical: moderateScale(5), borderWidth: 0.5, borderColor: colors.mediumBlue, borderStyle: 'solid', borderRadius: moderateScale(3) }}>
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>30 {Strings.RS}</Text>
                    </View>
                </View>


                <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(5), alignItems: 'center' }}>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                            }).then(image => {

                                this.setState({ image: image.path })
                            });
                        }}
                        style={{ backgroundColor: '#d1cfcf', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(25), height: responsiveHeight(5) }} >
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.upload}</Text>
                    </Button>
                    <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), marginVertical: moderateScale(3), color: colors.black, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.uploadConversionImage}</Text>
                </View>


            </View>
        )
    }

    render() {
        const { isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.payment} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.payment} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(Wallet);
