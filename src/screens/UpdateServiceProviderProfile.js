import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform, StyleSheet, Switch
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button, Radio } from 'native-base';
import { login, setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader';
import { removeItem } from '../actions/MenuActions';
import StepIndicator from 'react-native-step-indicator';
import Checkbox from 'react-native-custom-checkbox';
import { BASE_END_POINT } from '../AppConfig'
import RadioButton from 'radio-button-react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import AsyncStorage from '@react-native-community/async-storage'
import { RNToasty } from 'react-native-toasty'
import MapView, { Marker } from 'react-native-maps';
import InputValidations from '../common/InputValidations'
import axios from 'axios'
import strings from '../assets/strings';
import ParallaxScrollView from 'react-native-parallax-scroll-view';



const countryKeys = [
    // this is the parent or 'item'
    {
        name: 'Countries Key',
        id: 0,
        // these are the children or 'sub items'
        children: [
            {
                name: '966',
                id: 966,
            },
            {
                name: '971',
                id: 971,
            },
            {
                name: '965',
                id: 965,
            },
            {
                name: '973',
                id: 973,
            },
            {
                name: '968',
                id: 968,
            },
        ],
    },
];



const items5 = [
    // this is the parent or 'item'
    {
        name: 'Sub categories',
        id: 1,
        // these are the children or 'sub items'
        children: [
            {
                name: 'Sub category 1',
                id: 10,
            },
            {
                name: 'Sub category 1',
                id: 17,
            },
        ],
    },
]

class UpdateServiceProvider extends Component {

    state = {
        currentPage: 0,
        providerType: '', //select type company or individual
        companyType: '', companyTypeUpdt: ' ',
        name: '', nameUpdt: ' ',
        address: '', addressUpdt: ' ',
        selectCountryKey: '966',
        countries: [],
        logisticsCharitableType: 0, logisticsDeliveryType: 0, logisticsMapType: 0,
        selectCountry: Strings.selectCountry,
        selectCity: Strings.selectCity,
        selectRegion: Strings.selectRegion, selectRegionUpdt: ' ',
        cities: [],
        selectAdsCategoryId: '', selectAdsCategory: '', selectAdsCategoryUpdt: ' ',
        selectSubCategory: '', selectSubCategoryId: '', selectSubCategoryUpdt: ' ',
        //selectCity: null,
        area: [],
        ads: [],
        //selectArea: null,
        countryKey: '', countryKeyUpdt: ' ',
        phone: '', phoneUpdt: ' ',
        email: '', emailUpdt: ' ',
        image: null,
        password: '', passwordUpdt: ' ',
        hidePassword: false,
        confirmPassword: '', confirmPasswordUpdt: ' ',
        hideConfirmPassword: false,
        description: '', descriptionUpdt: ' ',
        logisticsType: 0,
        employeeNumbers: '', employeeNumbersUpdt: ' ',
        creationYear: '', creationYearUpdt: ' ',
        commercialRegister: '', commercialRegisterUpdt: ' ',
        primarEducation: '', primarEducationUpdt: ' ',
        intermediatEducation: '', intermediatEducationUpdt: ' ',
        universitdEucation: '', universitdEucationUpdt: ' ',
        categories: [],
        selectMainCategory: null,
        subCategories: [],
        selectSubCategoriesIds: [],
        selectSubCategory: null,
        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        selectCountryID: ' ',
        countries: [{
            name: 'Countries',
            id: 0
        }],
        selectCityID: '',
        cities: [{
            name: 'Cities',
            id: 0
        }],
        selectRegionID: ' ',
        regions: [{
            name: 'Regions',
            id: 0
        }],
        loginLoading: false,
        discount: 0,
        maroofAccountSwitch: this.props.currentUser.data.account_maroof == 'no' ? false:true,
        freelanceAccountSwitch: this.props.currentUser.data.account_freelancer == 'no' ? false:true,
    }

    componentDidMount() {

        const { isRTL, currentUser } = this.props
        console.log(currentUser.data.account_freelancer)
        var currentLang = ''
        if (isRTL) { currentLang = 'ar' } else { currentLang = 'en' }
        enableSideMenu(false, this.props.isRTL)
        this.getCountries()
        this.getAds()
        this.getCategories()

        var currentLocation = { latitude: currentUser.data.lat ? parseInt(currentUser.data.lat) : 0, longitude: currentUser.data.lng ? parseInt(currentUser.data.lng) : 0, latitudeDelta: 0.015, longitudeDelta: 0.0121, }


        var subCategoriesIds = []
        currentUser.data.sub_categories.map((item) => {
            subCategoriesIds = [...subCategoriesIds, { name: isRTL ? item.ar_name : item.en_name, id: item.id }]
        })
        console.log(currentUser.data)
        this.setState({
            providerType: currentUser.data.provider_type,
            companyType: currentUser.data.provider_company_type,
            name: currentUser.data.name,
            //address: currentUser.data.address,
            selectSubCategoriesIds: subCategoriesIds,
            selectCountryId: currentUser.data.country_id,
            selectCountryName: currentUser.data.country_name,
            email: currentUser.data.email,
            selectCountryKey: currentUser.data.phone.substring(0, 3).toString(),
            phone: currentUser.data.phone.split(currentUser.data.phone.substring(0, 3).toString())[1],
            //phone: currentUser.data.phone,
            currentImage: currentUser.data.image,
            selectCountryID: currentUser.data.country_id,
            selectCountry: currentUser.data.country_name,
            selectCityID: currentUser.data.city_id,
            selectCity: currentUser.data.city_name,
            description: currentUser.data.bio,
            logisticsCharitableType: parseInt(currentUser.data.charitable),
            logisticsDeliveryType: parseInt(currentUser.data.delivery),
            logisticsMapType: currentUser.data.map,
            employeeNumbers: currentUser.data.emp_no,
            creationYear: currentUser.data.creation_year,
            commercialRegister: currentUser.data.commerical_no,
            selectAdsCategoryId: currentUser.data.ads_category != null ? currentUser.data.ads_category.id : '',
            selectAdsCategory: currentUser.data.ads_category != null ? currentUser.data.ads_category[currentLang + '_name'] : '',
            selectCategoryId: currentUser.data.category.id,
            selectMainCategory: currentUser.data.category[currentLang + '_name'],
            selectSubCategoryId: currentUser.data.sub_categories[0]['id'],
            selectSubCategory: currentUser.data.sub_categories[0][currentLang + '_name'],
            location: { latitude: currentUser.data.lat ? parseInt(currentUser.data.lat) : 0, longitude: currentUser.data.lng ? parseInt(currentUser.data.lng) : 0, latitudeDelta: 0.015, longitudeDelta: 0.0121, },
            discount: currentUser.data.discount ? currentUser.data.discount : 0,
        })
        //this.getSubCategories(currentUser.data.category.id)
    }

    componentWillUnmount() {
        this.props.removeItem()

    }

    getCountries = () => {
        this.setState({ loginLoading: true })
        axios.get(`${BASE_END_POINT}locations/countries`)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newCountries = [{
                        name: Strings.countries,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ countries: newCountries })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    getCities = (countryId) => {

        this.setState({ loginLoading: false })

        axios.get(`${BASE_END_POINT}locations/cities/` + countryId)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newCities = [{
                        name: Strings.cities,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ cities: newCities })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    getAds = () => {
        axios.get(`${BASE_END_POINT}get-ads-categories`)
            .then(response => {

                var newAds = [{
                    name: Strings.adsCategory,
                    id: 0, children: response.data.data
                }]
                this.setState({ ads: newAds })
                // this.setState({ ads: response.data.data, })
            })
            .catch(error => {

                this.setState({ ads404: true, })
            })
    }

    getRegions = (cityId) => {
        this.setState({ loginLoading: false })

        axios.get(`${BASE_END_POINT}locations/regions/` + cityId)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newRegions = [{
                        name: Strings.regions,
                        id: 0, children: response.data.data
                    }]

                    this.setState({ regions: newRegions })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    ///////////// Get Categories
    getCategories = () => {
        const { isRTL } = this.props
        this.setState({ loginLoading: false })
        axios.get(`${BASE_END_POINT}categories`)
            .then(response => {
                //Alert.alert('Done')
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {

                    var categories = []
                    response.data.data.map((item) => {
                        categories = [...categories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
                    })

                    var newCategories = [{
                        name: Strings.mainCategory,
                        id: 0, children: categories
                    }]

                    this.setState({ categories: newCategories })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {
                //Alert.alert('Error   ',error)

                this.setState({ loginLoading: false })
            })
    }
    ///////// 
    getSubCategories = (catId) => {
        const { isRTL } = this.props
        this.setState({ loginLoading: false })
        axios.get(`${BASE_END_POINT}sub_categories/${catId}`)
            .then(response => {
                //Alert.alert('Done')
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var subCategories = []
                    response.data.data.map((item) => {
                        subCategories = [...subCategories, { name: isRTL ? item.name_ar : item.name_en, id: item.id }]
                    })


                    var newSubCategories = [{
                        name: Strings.subCategory,
                        id: 0, children: subCategories
                    }]

                    var selectSubCategoriesIds = this.state.selectSubCategoriesIds
                    subCategories.map(item => {
                        selectSubCategoriesIds.push(item)
                    })

                    var obj = {};

                    for (var i = 0, len = selectSubCategoriesIds.length; i < len; i++) {
                        obj[selectSubCategoriesIds[i]['id']] = selectSubCategoriesIds[i];
                    }
                    selectSubCategoriesIds = new Array();
                    for (var key in obj) {
                        selectSubCategoriesIds.push(obj[key]);
                    }

                    selectSubCategoriesIds = selectSubCategoriesIds.filter((x, i, a) => a.indexOf(x) == i)

                    this.setState({ subCategories: newSubCategories, selectSubCategoriesIds: selectSubCategoriesIds })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {
                //Alert.alert('Error   ',error)

                this.setState({ loginLoading: false })
            })
    }



    update = () => {
        const { discount, name, image, phone, email, selectCityID, selectAdsCategoryId, selectSubCategoryId, providerType, description, logisticsCharitableType, logisticsDeliveryType, logisticsMapType, employeeNumbers, creationYear, commercialRegister, companyType, location, selectCountryKey, selectSubCategoriesIds, maroofAccountSwitch, freelanceAccountSwitch  } = this.state


        if (name.replace(/\s/g, '').length && phone.replace(/\s/g, '').length && email.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true) {
            this.setState({ loginLoading: true })
            var data = new FormData()
            data.append('name', name)
            data.append('email', email)
            data.append('code', selectCountryKey)
            data.append('phone', phone)
            //data.append('password', password)
            data.append('city_id', selectCityID)
            if (image){
            if (image) {
                data.append('image', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'photoImage'
                })
            }
        }

            data.append('job', 'مدرس')
            selectSubCategoriesIds.map(item => {
                data.append('sub_categories[]', item.id)
            })
            data.append('bio', description)
            data.append('lat', location.latitude)
            data.append('lng', location.longitude)
            data.append('discount', '')
            //data.append('emp_no', employeeNumbers)
           // data.append('creation_year', creationYear)
           if ((commercialRegister.toString()).replace(/\s/g, '') != '') {
            data.append('commerical_no', commercialRegister)
        }
        if (maroofAccountSwitch == true) { data.append('account_maroof', 'yes') } else { data.append('account_maroof', 'no') }
        if (freelanceAccountSwitch == true) { data.append('account_freelancer', 'yes') } else { data.append('account_freelancer', 'no') }
        console.log('maroofAccountSwitch', maroofAccountSwitch )
        console.log('freelanceAccountSwitch',freelanceAccountSwitch)
            data.append('map', logisticsMapType)
            data.append('delivery', logisticsDeliveryType)
            data.append('charitable', logisticsCharitableType)
            data.append('provider_type', providerType)
            data.append('provider_company_type', companyType)
            data.append('ads_category', selectAdsCategoryId)

            if (this.props.currentUser.data.is_special == "1") {
                data.append('discount', discount)
            }


            axios.post(`${BASE_END_POINT}user/update/profile`, data, {
                headers: {
                    //'Content-Type': 'application/json',
                    "Authorization": "Bearer " + this.props.currentUser.data.token
                    //'Content-Type': 'multipart/form-data',
                },
            })
                .then(response => {
                    this.setState({ loginLoading: false })
                    const res = response.data
                    if ('data' in res) {

                        AsyncStorage.setItem('USER', JSON.stringify(res))
                        RNToasty.Success({ title: Strings.dataUpdatedSuccessfuly })
                        this.props.setUser(res)
                        resetTo('Home')
                        //  this.setState({ currentPage: 4 })
                    } else {
                        if (response.data.value == true) {
                            RNToasty.Success({ title: res.message })
                            resetTo('Home')
                            // this.setState({ currentPage: 4 })
                        } else {

                            RNToasty.Error({ title: res.msg })
                        }
                    }
                })
                .catch(error => {
                    if (error.response.status == 401) {
                        RNToasty.Error({ title: 'Session expired' })
                    } else {
                        RNToasty.Error({ title: Strings.errorInUpdate })
                    }


                    this.setState({ loginLoading: false })

                })
        }
    }

    discountInput = () => {
        const { discount } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(0), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', }}>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.discount}</Text>
                    </View>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ discount: val, }) }}
                        placeholder={Strings.enterDiscountRate} value={discount} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />

                </View>
                {discount.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    nameInput = () => {
        const { name, nameUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(0), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', }}>
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.name}</Text>
                    </View>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val, nameUpdt: val }) }}
                        placeholder={Strings.name} value={name} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />

                </View>
                {nameUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    addressInput = () => {
        const { address, addressUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.address}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ address: val, addressUpdt: val }) }}
                        placeholder={Strings.address} value={address} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }}
                    />
                </View>
                {addressUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    countryKeyInput = () => {
        const { countryKey, selectCountryKey } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.countryKey}</Text>*/}
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }

                        }}
                        items={countryKeys}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        searchPlaceholderText={Strings.search}
                        confirmText={Strings.close}
                        selectText={this.state.selectCountryKey}
                        hideSearch
                        expandDropDowns
                        showDropDowns={false}
                        readOnlyHeadings={true}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountryKey: selectedItems[0].name });


                        }
                        }
                    />
                </View>
                {selectCountryKey.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    emailInput = () => {
        const { email, emailUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.email}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val, emailUpdt: val }) }}
                        placeholder={Strings.email} value={email} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {emailUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    phoneInput = () => {
        const { phone, phoneUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.phone}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ phone: val, phoneUpdt: val }) }}
                        placeholder={Strings.phone}
                        //value={phone.split(phone.substring(0, 3).toString())[1]}
                        value={phone}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phoneUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword, passwordUpdt } = this.state
        return (
            <Animatable.View animation="slideInLeft" duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.password}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderRadius: moderateScale(0), borderBottomWidth: 1, borderBottomColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val, passwordUpdt: val }) }}
                        placeholder={Strings.password} value={password} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(79), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hidePassword: !hidePassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hidePassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {passwordUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    confiemPasswordInput = () => {
        const { isRTL } = this.props
        const { confirmPassword, hideConfirmPassword, confirmPasswordUpdt } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>

                {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.confirmPassword}</Text>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderRadius: moderateScale(0), borderBottomWidth: 1, borderBottomColor: '#cccbcb' }}>
                    <TextInput
                        secureTextEntry={hideConfirmPassword}
                        onChangeText={(val) => { this.setState({ confirmPassword: val, confirmPasswordUpdt: val }) }}
                        placeholder={Strings.confirmPassword} value={confirmPassword} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(79), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hideConfirmPassword: !hideConfirmPassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hideConfirmPassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {confirmPasswordUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    imageInput = () => {
        const { isRTL } = this.props
        const { image, currentImage } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), width: responsiveWidth(85), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>

                {/*<Text style={{ marginHorizontal: moderateScale(2), fontFamily: isRTL ? arrabicFont : englishFont }}> {Strings.selectImage}</Text>*/}
                <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                            }).then(image => {

                                this.setState({ image: image.path })
                            });
                        }}
                        style={{ backgroundColor: '#d1cfcf', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(25), height: responsiveHeight(5) }} >
                        <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.uploadImage}</Text>
                    </Button>

                    <FastImage
                        source={{ uri: image ? image : currentImage }}
                        style={{ borderRadius: moderateScale(3), marginHorizontal: moderateScale(10), width: responsiveWidth(30), height: responsiveHeight(20) }}
                    />

                </View>
            </View>
        )
    }

    descriptionInpu = () => {
        const { isRTL } = this.props
        const { description, descriptionUpdt } = this.state

        return (
            <View style={{ alignSelf: 'center', width: responsiveWidth(90) }} >

                <View style={{ padding: moderateScale(4), width: responsiveWidth(90), borderRadius: moderateScale(10), marginTop: moderateScale(5), marginHorizontal: moderateScale(5), alignSelf: 'center', }} >
                    {/*<Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue }}>{Strings.description}</Text>*/}
                    <Icon name="pencil" type="Octicons" style={{ fontSize: 15, alignSelf: isRTL ? 'flex-start' : 'flex-end', }} />
                </View>


                <TextInput
                    placeholder={Strings.description}
                    multiline
                    onChangeText={(val) => { this.setState({ description: val, descriptionUpdt: val }) }}
                    value={description}
                    style={{ padding: moderateScale(5), textAlignVertical: 'top', marginTop: moderateScale(3), width: responsiveWidth(90), height: responsiveHeight(20), borderRadius: moderateScale(5), borderColor: colors.lightGray, borderWidth: 1 }}
                />
            </View>
        )
    }


    logisticsTypeRadioButtons = () => {
        const { isRTL } = this.props
        const { logisticsType } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(3), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
                <RadioButton
                    innerCircleColor={colors.orange}
                    currentValue={this.state.logisticsCharitableType}
                    value={1}
                    onPress={(value) => { this.state.logisticsCharitableType == 0 ? this.setState({ logisticsCharitableType: 1 }) : this.setState({ logisticsCharitableType: 0 }) }}
                >
                    <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.charitableOffers}</Text>
                </RadioButton>

                <View style={{ marginVertical: moderateScale(5) }}>
                    <RadioButton
                        innerCircleColor={colors.orange}
                        currentValue={this.state.logisticsDeliveryType}
                        value={1}
                        onPress={(value) => { this.state.logisticsDeliveryType == 0 ? this.setState({ logisticsDeliveryType: 1 }) : this.setState({ logisticsDeliveryType: 0 }) }}
                    >
                        <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.deliveryServices}</Text>
                    </RadioButton>
                </View>

                <RadioButton
                    innerCircleColor={colors.orange}
                    currentValue={this.state.logisticsMapType == 'LoMap' ? 1 : 0}
                    value={1}
                    onPress={(value) => { this.state.logisticsMapType == 0 ? this.setState({ logisticsMapType: 1 }) : this.setState({ logisticsMapType: 0 }) }}
                >
                    <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.locationOnMap}</Text>
                </RadioButton>
            </View>
        )
    }

    maroofAccountSwitch = () => {
        const { isRTL } = this.props
        const { maroofAccountSwitch } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(85), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(7) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.areYouHaveAccountInMaroof}</Text>
                <Switch
                    onValueChange={(val) => this.setState({ maroofAccountSwitch: !maroofAccountSwitch })}
                    value={maroofAccountSwitch}
                    trackColor={{ true: colors.darkBlue, false: 'grey' }} />
            </View>
        )
    }

    freelanceAccountSwitch = () => {
        const { isRTL } = this.props
        const { freelanceAccountSwitch } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(85), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', marginTop: moderateScale(7) }}>
                <Text style={{ marginBottom: moderateScale(3), fontFamily: isRTL ? arrabicFont : englishFont, color: colors.black, fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}> {Strings.areYouHaveAccountInFreelance}</Text>
                <Switch
                    onValueChange={(val) => this.setState({ freelanceAccountSwitch: !freelanceAccountSwitch })}
                    value={freelanceAccountSwitch}
                    trackColor={{ true: colors.darkBlue, false: 'grey' }} />
            </View>
        )
    }

    employeeNumbersInput = () => {
        const { employeeNumbers, employeeNumbersUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.employeeNumbers}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ employeeNumbers: val, employeeNumbersUpdt: val }) }}
                        placeholder={Strings.employeeNumbers} value={employeeNumbers} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {employeeNumbersUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    creationYearInput = () => {
        const { creationYear } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.creationYear}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ creationYear: val }) }}
                        placeholder={Strings.creationYear} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*creationYear.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }


    commercialRegisterInput = () => {
        const { commercialRegister } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.commercialRegister}</Text>*/}
                    <TextInput
                        onChangeText={(val) => { this.setState({ commercialRegister: val }) }}
                        placeholder={Strings.commercialRegister} value={commercialRegister} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*commercialRegister.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    primarEducationInput = () => {
        const { primarEducation } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.primarEducation}</Text>
                    <TextInput
                        onChangeText={(val) => { this.setState({ primarEducation: val }) }}
                        placeholder={Strings.primarEducation} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*primarEducation.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    intermediatEducationInput = () => {
        const { intermediatEducation } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.intermediatEducation}</Text>
                    <TextInput
                        onChangeText={(val) => { this.setState({ intermediatEducation: val }) }}
                        placeholder={Strings.intermediatEducation} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*intermediatEducation.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }

    universitdEucationInput = () => {
        const { universitdEucation } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(0), }}>{Strings.universitdEucation}</Text>
                    <TextInput
                        onChangeText={(val) => { this.setState({ universitdEucation: val }) }}
                        placeholder={Strings.universitdEucation} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*universitdEucation.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }



    country_city_areas_pickerInputs = () => {
        const { countries, selectCountry, cities, selectCity, selectRegion, area, selectArea } = this.state
        const { isRTL } = this.props
        return (
            <View>
                {/*countries */}
                <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectCountry}</Text>*/}
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.countries}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        searchPlaceholderText={Strings.search}
                        confirmText={Strings.close}
                        selectText={this.state.selectCountry}
                        hideSearch
                        expandDropDowns
                        showDropDowns={false}
                        readOnlyHeadings={true}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            // console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountry: selectedItems[0].name, selectCountryID: selectedItems[0].id });

                            this.getCities(selectedItems[0].id)
                        }
                        }
                    />

                </Animatable.View>

                {/*city */}
                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>
                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectCity}</Text>*/}

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.cities}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        searchPlaceholderText={Strings.search}
                        confirmText={Strings.close}
                        selectText={this.state.selectCity ? this.state.selectCity : Strings.selectCity}
                        showDropDowns={false}
                        readOnlyHeadings={true}
                        hideSearch
                        expandDropDowns
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCity: selectedItems[0].name, selectCityID: selectedItems[0].id });
                            //this.getRegions(selectedItems[0].id);
                        }
                        }
                    />

                </Animatable.View>


                {/*areas */}
                {/*<Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>

                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.selectArea}</Text>

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                        }}
                        items={this.state.regions}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        searchPlaceholderText='search...'
                        selectText={this.state.selectRegion ? this.state.selectRegion : Strings.selectRegion}
                        showDropDowns={true}
                        readOnlyHeadings={true}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectRegion: selectedItems[0].name, selectRegionID: selectedItems[0].id });
                        }
                        }
                    />

                    </Animatable.View>*/}
            </View>
        )
    }


    mainCategory_subCategory_pickerInputs = () => {
        const { mainCategories, selectSubCategory, selectAdsCategoryUpdt, selectSubCategoriesIds } = this.state
        const { isRTL, currentUser } = this.props


        return (
            <View>
                {/*mainCategories */}
                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.adsCategory}</Text>*/}

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(60), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(20), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.ads}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        searchPlaceholderText={Strings.search}
                        confirmText={Strings.close}
                        selectText={this.state.selectAdsCategory ? this.state.selectAdsCategory : Strings.adsCategory}
                        showDropDowns={false}
                        readOnlyHeadings={true}
                        modalWithTouchable

                        hideSearch
                        expandDropDowns
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            // console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectAdsCategory: selectedItems[0].name, selectAdsCategoryId: selectedItems[0].id, selectAdsCategoryUpdt: selectedItems[0].name });
                        }
                        }
                    />

                    {selectAdsCategoryUpdt.length == 0 &&
                        <></>
                        /*<Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                    }
                </Animatable.View>


                <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#cccbcb', }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.mainCategory}</Text>*/}

                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(16), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.categories}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        confirmText={Strings.close}
                        selectText={this.state.selectMainCategory ? this.state.selectMainCategory : Strings.mainCategory}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable

                        hideSearch
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            // console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectMainCategory: selectedItems[0].name, selectCategoryId: selectedItems[0].id });
                            this.getSubCategories(selectedItems[0].id)
                        }
                        }
                    />

                </Animatable.View>

                {/*Sub categories */}
                {/* <Animatable.View animation={"slideInLeft"} duration={1500} style={{ marginVertical: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center' }}>

                    {/*<Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginBottom: moderateScale(5), }}>{Strings.subCategory}</Text>*}
                    <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 1, borderColor: '#cccbcb' }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(85), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                                selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                container: { height: responsiveHeight(80), position: 'absolute', width: responsiveWidth(85), top: responsiveHeight(16), alignSelf: 'center' },
                                button:{backgroundColor:colors.blueTabs}
                            }}
                            items={this.state.subCategories}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectSubCategory ? this.state.selectSubCategory : Strings.subCategory}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            
                            searchPlaceholderText={Strings.search}
                            confirmText={Strings.close}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                               // console.log("SubCATTTT   ", selectedItems[0].name)
                                this.setState({ selectSubCategory: selectedItems[0].name, selectSubCategoryId: selectedItems[0].id, selectSubCategoryUpdt: selectedItems[0].name });

                            }
                            }
                        />
                    </View>
                    {selectSubCategoryUpdt.length == 0 &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                </Animatable.View>*/}


                <View style={{ width: responsiveWidth(90), flexDirection: isRTL ? 'row-reverse' : 'row', flexWrap: 'wrap', alignItems: 'center', alignSelf: 'center', marginVertical: moderateScale(5), paddingHorizontal: moderateScale(1) }}>
                    {selectSubCategoriesIds.map((val, elem) => (
                        <View>

                            <View style={{ width: responsiveWidth(28.5), height: responsiveHeight(6), marginHorizontal: moderateScale(0.5), justifyContent: 'center', marginVertical: moderateScale(1), alignItems: 'center', borderRadius: moderateScale(2), backgroundColor: colors.darkBlue }}>
                                <Text style={{ paddingHorizontal: moderateScale(1), paddingVertical: moderateScale(0), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(5) }}>{val.name}</Text>
                            </View>

                            <TouchableOpacity onPress={() => this.removeElement(selectSubCategoriesIds, elem)}
                                style={{ position: 'absolute', top: moderateScale(-3), backgroundColor: 'white', borderRadius: moderateScale(3) }}>
                                <Icon name='delete' type='AntDesign' style={{ fontSize: responsiveFontSize(7), color: 'red' }} />
                            </TouchableOpacity>
                        </View>

                    ))}

                </View>




            </View>
        )
    }

    removeElement(array, index) {

        if (index > -1) {
            array.splice(index, 1);
        }
        this.setState({ selectSubCategoriesIds: array })
    }

    map = () => {
        const { isRTL } = this.props
        const { location } = this.state
        return (
            <View>
                <Text style={{ color: colors.darkBlue, alignSelf: isRTL ? 'flex-end' : 'flex-start', marginTop: moderateScale(10), marginHorizontal: moderateScale(7), fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.selectYouLocation}</Text>
                <MapView
                    onPress={(coordinate) => {
                        //Alert.alert(coordinate.nativeEvent.coordinate.latitude+"")
                        this.setState({
                            location: {
                                latitude: coordinate.nativeEvent.coordinate.latitude,
                                longitude: coordinate.nativeEvent.coordinate.longitude,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }
                        })
                    }}
                    style={{ alignSelf: 'center', width: responsiveWidth(100), height: responsiveHeight(60), marginVertical: moderateScale(5) }}
                    region={location}
                >
                    <Marker
                        coordinate={location}
                        image={require('../assets/imgs/marker.png')}
                    />
                </MapView>
            </View>
        )
    }



    page1 = () => {
        const { isRTL } = this.props
        const { type, providerType } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100) }} >

                <Icon name="pencil" type="Octicons" style={{ fontSize: 15, marginHorizontal: moderateScale(8), alignSelf: isRTL ? 'flex-start' : 'flex-end', marginTop: moderateScale(6) }} />

                <View style={{ marginTop: moderateScale(15), marginHorizontal: moderateScale(8), width: responsiveWidth(80), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }} >
                        <RadioButton
                            innerCircleColor={colors.orange}
                            currentValue={this.state.providerType}
                            value={'company'}
                            onPress={(value) => { this.setState({ providerType: value }) }} >
                        </RadioButton>
                        <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.company}</Text>
                    </View>

                    <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row' }} >
                        <RadioButton
                            innerCircleColor={colors.orange}
                            currentValue={this.state.providerType}
                            value={'one'}
                            onPress={(value) => { this.setState({ providerType: value }) }} >
                        </RadioButton>
                        <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.individual}</Text>
                    </View>

                </View>

                {providerType == 'company' &&
                    <>
                        <View style={{ borderBottomColor: colors.lightGray, borderBottomWidth: 2, marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: isRTL ? 'flex-end' : 'flex-start', }}>
                            <Text style={{ paddingBottom: moderateScale(5), paddingHorizontal: moderateScale(8), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontSize: responsiveFontSize(6), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont, borderBottomColor: '#80ad30', borderBottomWidth: 2 }}>{Strings.selectCompanyType}</Text>
                        </View>
                        <Icon name="pencil" type="Octicons" style={{ fontSize: 15, marginHorizontal: moderateScale(8), alignSelf: isRTL ? 'flex-start' : 'flex-end', marginTop: moderateScale(-5) }} />
                    </>
                }
                {providerType == 'company' &&
                    <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(8), width: responsiveWidth(80), alignSelf: isRTL ? 'flex-end' : 'flex-start', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20) }} >
                            <RadioButton
                                innerCircleColor={colors.orange}
                                currentValue={this.state.companyType}
                                value={'MNC'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >
                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.holding}</Text>
                        </View>

                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20) }} >
                            <RadioButton
                                innerCircleColor={colors.orange}
                                currentValue={this.state.companyType}
                                value={'LTD'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >
                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.input}</Text>
                        </View>
                    </View>
                }
                {providerType == 'company' &&
                    <View style={{ marginTop: moderateScale(5), marginHorizontal: moderateScale(8), width: responsiveWidth(80), alignSelf: isRTL ? 'flex-end' : 'flex-start', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20) }} >
                            <RadioButton
                                innerCircleColor={colors.orange}
                                currentValue={this.state.companyType}
                                value={'PVT'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >

                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }} >{Strings.limited}</Text>
                        </View>

                        <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', width: responsiveWidth(20) }} >
                            <RadioButton innerCircleColor={colors.orange}
                                currentValue={this.state.companyType}
                                value={'Registered'}
                                onPress={(value) => { this.setState({ companyType: value }) }} >

                            </RadioButton>
                            <Text style={{ marginTop: moderateScale(1), fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2) }}>{Strings.starch}</Text>
                        </View>
                    </View>
                }
                <Button onPress={() => {
                    this.setState({ currentPage: 1 })
                }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(25), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                </Button>


            </Animatable.View>
        )
    }

    page2 = () => {
        const { isRTL } = this.props
        const { type, name, selectCity } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100), }} >

                <View style={{ borderWidth: 1, width: responsiveWidth(90), borderRadius: moderateScale(5), borderColor: colors.lightGray, alignSelf: 'center', marginTop: moderateScale(10) }}>
                    <Icon name="pencil" type="Octicons" style={{ fontSize: 17, marginHorizontal: moderateScale(7), alignSelf: isRTL ? 'flex-start' : 'flex-end', marginTop: moderateScale(4) }} />
                    {this.nameInput()}

                    {this.country_city_areas_pickerInputs()}
                </View>

                <View style={{ marginBottom: moderateScale(15), width: responsiveWidth(85), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 0 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'gray' }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {

                        if (!name.replace(/\s/g, '').length) { this.setState({ nameUpdt: '' }) }

                        if (!selectCity.replace(/\s/g, '').length) { this.setState({ selectCity: '' }) }
                        if (name.replace(/\s/g, '').length && selectCity.replace(/\s/g, '').length) {
                            this.setState({ currentPage: 2 })
                        }



                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>
            </Animatable.View>
        )
    }


    page3 = () => {
        const { isRTL, currentUser } = this.props
        const { type, email, phone } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100), }} >

                <View style={{ width: responsiveWidth(90), alignSelf: 'center', borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(5), marginTop: moderateScale(10) }}>
                    <Icon name="pencil" type="Octicons" style={{ fontSize: 17, marginHorizontal: moderateScale(7), alignSelf: isRTL ? 'flex-start' : 'flex-end', marginTop: moderateScale(4) }} />
                    {this.emailInput()}
                    {this.countryKeyInput()}
                    {this.phoneInput()}
                    {currentUser.data.is_special == "1" && this.discountInput()}
                    {this.imageInput()}
                    {/*this.passwordInput()*/}
                    {/*this.confiemPasswordInput()*/}
                </View>

                <View style={{ marginBottom: moderateScale(15), width: responsiveWidth(85), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 1 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'gray' }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {
                        if (!email.replace(/\s/g, '').length) { this.setState({ emailUpdt: '' }) }
                        if (InputValidations.validateEmail(email) == false) { RNToasty.Error({ title: Strings.emailNotValid }) }
                        if (!phone.replace(/\s/g, '').length) { this.setState({ phoneUpdt: '' }) }


                        if (email.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true && phone.replace(/\s/g, '').length) {
                            this.setState({ currentPage: 3 })
                        }


                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>

            </Animatable.View>
        )
    }


    page4 = () => {
        const { isRTL } = this.props
        const { employeeNumbers, selectAdsCategory, selectSubCategory, selectSubCategoriesIds } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1500} style={{ width: responsiveWidth(100), }}>
                {this.descriptionInpu()}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', padding: moderateScale(4), width: responsiveWidth(90), borderRadius: moderateScale(10), marginTop: moderateScale(7), marginHorizontal: moderateScale(5), alignSelf: 'center', justifyContent: 'space-between' }} >
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue }}>{Strings.additionalLogisticsServices}</Text>
                    <Icon name="pencil" type="Octicons" style={{ fontSize: 15, alignSelf: isRTL ? 'flex-start' : 'flex-end', }} />
                </View>
                <View style={{ alignSelf: 'center', paddingVertical: moderateScale(3), width: responsiveWidth(90), borderRadius: moderateScale(5), borderWidth: 1, borderColor: colors.lightGray }}>
                    {this.logisticsTypeRadioButtons()}
                    {/*this.employeeNumbersInput()*/}
                    {/*this.creationYearInput()*/}
                    {this.commercialRegisterInput()}
                    {this.maroofAccountSwitch()}
                    {this.freelanceAccountSwitch()}
                    
                    {/*this.primarEducationInput()*/}
                    {/*this.intermediatEducationInput()/*}
                    {/*this.universitdEucationInput()*/}
                    <View style={{ borderRadius: moderateScale(10), marginTop: moderateScale(5), marginHorizontal: moderateScale(5), alignSelf: isRTL ? 'flex-end' : 'flex-start' }} >
                        <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont, color: colors.darkBlue, fontSize: responsiveFontSize(6), textDecorationLine: 'underline' }}>{Strings.chooseAppropriateService}</Text>
                    </View>
                    {this.mainCategory_subCategory_pickerInputs()}
                </View>
                {this.map()}


                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', alignItems: 'center' }}>


                    <Button onPress={() => {
                        this.setState({ currentPage: 2 })
                    }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: 'white' }} >
                        <Text style={{ color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {

                        //if (!employeeNumbers.replace(/\s/g, '').length) { this.setState({ employeeNumbersUpdt: '' }) }
                        //if (!selectAdsCategory.replace(/\s/g, '').length) { this.setState({ selectAdsCategoryUpdt: '' }) }
                        //if (!selectSubCategory.replace(/\s/g, '').length) { this.setState({ selectSubCategoryUpdt: '' }) }
                        if (selectSubCategoriesIds.length <= 0) { RNToasty.Error({ title: Strings.mustChooseSubCategoryatLeast }) }
                        if (selectSubCategoriesIds.length > 0) {
                            this.update()
                        }
                        //this.setState({currentPage:3})
                    }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.save}</Text>
                    </Button>
                </View>
            </Animatable.View>
        )
    }

    showLoading = () => {
        return (
            this.state.loginLoading &&
            <LoadingDialogOverlay title={Strings.wait} />
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { currentPage } = this.state
        return (
            <View>

                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {currentPage == 2 && this.page3()}
                {currentPage == 3 && this.page4()}
                {this.showLoading()}

            </View>
        )
    }


    render() {
        const { isRTL, userToken } = this.props;

        return (
            /* <ReactNativeParallaxHeader
                 scrollViewProps={{ showsVerticalScrollIndicator: false }}
                 headerMinHeight={responsiveHeight(10)}
                 headerMaxHeight={responsiveHeight(35)}
                 extraScrollHeight={20}
                 navbarColor={colors.darkBlue}
                 backgroundImage={require('../assets/imgs/header.png')}
                 backgroundImageScale={1.2}
                 renderNavBar={() => <CollaspeAppHeader back title={Strings.updateServiceProvider} />}
                 renderContent={() => this.content()}
                 containerStyle={{ flex: 1 }}
                 contentContainerStyle={{ flexGrow: 1 }}
                 innerContainerStyle={{ flex: 1, }}
             />*/

            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.updateServiceProvider} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: 'green',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: 'green',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: 'green',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: 'green',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: 'green',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013'
}

const mapDispatchToProps = {
    login,
    removeItem,
    setUser
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    currentUser: state.auth.currentUser
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(UpdateServiceProvider);

