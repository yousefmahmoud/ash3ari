import React, { Component } from 'react';
import { View, RefreshControl, Alert, FlatList, ScrollView, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import ListFooter from '../common/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import { enableSideMenu, pop } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header'
import PageTitleLine from '../components/PageTitleLine'
import WebView from 'react-native-webview'
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ImagePicker from 'react-native-image-crop-picker';
import { RNToasty } from 'react-native-toasty'
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class UploadTransferReceipt extends Component {

    page = 1;
    list = [];
    state = {
        networkError: null,
        rooms: [],
        refresh: false,
        loading: false,
        pages: null,
        paymentType: 0,
        currentPage: 0,
        sandboxUrl: '',
        image: null,
        imageHeight: 0,
        note: ''

    }



    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
    }
    componentWillUnmount() {
        this.props.removeItem('PAYMENT')
    }

    sendTransferReport = () => {
        const { transferType } = this.props.data
        const { image, note } = this.state

        this.setState({ loading: true })
        var data = new FormData()

        data.append('image', {
            uri: image,
            type: 'multipart/form-data',
            name: 'photoImage'
        })
        data.append('type', transferType)
        data.append('message', note)
        axios.post(`${BASE_END_POINT}user/send_report`, data, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                const res = response.data

                if (response.data.success == true) {
                    RNToasty.Success({ title: res.msg })
                    pop()
                    // this.setState({ loading: false })
                } else {

                    RNToasty.Error({ title: res.msg })
                }

            })
            .catch(error => {


                this.setState({ loading: false })

            })

    }

    page2() {
        const { sandboxUrl } = this.state
        const { isRTL } = this.props
        return (

            <WebView
                source={{ uri: sandboxUrl }}
                style={{ marginTop: 20, width: responsiveWidth(100), height: responsiveHeight(50) }}
            />

        )
    }

    page1 = () => {
        const { isRTL, barColor } = this.props
        const { paymentType, image, imageHeight } = this.state

        return (
            <View>

                <PageTitleLine title={this.props.data.data} iconName={this.props.data.data == Strings.payForExcellence ? 'star-o' : 'money'} iconType='FontAwesome' />





                <View style={{ alignSelf: 'center', width: responsiveWidth(80) }}>


                    <TouchableOpacity onPress={() => {
                        ImagePicker.openPicker({
                            width: 300,
                            height: 400,
                            cropping: true
                        }).then(image => {

                            this.setState({ image: image.path, imageHeight: responsiveWidth(30) })
                        });
                    }} style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: 'green', marginVertical: moderateScale(10), borderRadius: moderateScale(5), width: responsiveWidth(70), height: responsiveHeight(6.5), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.chooseTransferFile}</Text>
                    </TouchableOpacity>

                    <FastImage source={{ uri: image }} style={{ width: responsiveWidth(30), height: imageHeight, alignSelf: 'center', marginBottom: moderateScale(10) }} />

                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', backgroundColor: 'white', elevation: 0.2, marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(0), height: responsiveHeight(30), marginVertical: moderateScale(8) }}>
                        <TextInput
                            onChangeText={(val) => { this.setState({ note: val }) }}
                            placeholder={Strings.messageForWebsite} multiline style={{ fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left', width: responsiveWidth(90), height: responsiveHeight(30), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left', textAlignVertical: 'top', borderRadius: moderateScale(5), borderColor: colors.lightGray, borderWidth: 1, paddingHorizontal: moderateScale(3) }} />
                    </View>

                    <TouchableOpacity onPress={() => {
                        this.sendTransferReport()
                        //this.setState({currentPage:3})
                    }} style={{ padding: 0, marginHorizontal: moderateScale(5), marginVertical: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.send}</Text>
                    </TouchableOpacity>


                </View>

            </View>
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { paymentType, currentPage, loading } = this.state
        return (
            <>

                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {loading && <LoadingDialogOverlay title={Strings.wait} />}


            </>

        )
    }

    render() {
        const { isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.payment} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.payment} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadTransferReceipt);
