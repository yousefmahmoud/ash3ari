import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, StyleSheet, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push, pop, resetTo } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import InputValidations from '../common/InputValidations'
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import Dialog, { DialogContent, DialogTitle } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-crop-picker';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'



class MakeOrder extends Component {

    importance = [
        {
            name: Strings.selectImportance,
            id: 0,
            children: [
                {
                    name: strings.utmost,
                    id: 10,
                },
                {
                    name: strings.medium,
                    id: 18,
                },
                {
                    name: strings.normal,
                    id: 17,
                },

            ],
        },
    ];

    state = {
        name: '', nameUpdt: ' ',
        email: '', emailUpdt: ' ',
        phone: ' ',
        orderAddress: '', orderAddressUpdt: ' ',
        expectedTime: ' ', expectedTimeUpdt: ' ',
        estimatedBudget: ' ', estimatedBudgetUpdt: ' ',
        orderDescription: '', orderDescriptionUpdt: ' ',
        message: ' ',
        estimatedBudgetNumber: 0,
        categories: [],
        selectCategory: null,
        selectImportance: null,
        loading: false,
        showSentOrderDialog: false,
        image: null,
        fileName: ''
    }

    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
        this.getCategories()

    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }

    getCategories = () => {
        axios.get(BASE_END_POINT + 'categories')
            .then(response => {
                console.log('DONE categories    ', response.data.data)
                const res = response.data.data
                this.setState({
                    categories: [
                        {
                            name: Strings.categories,
                            id: 1,
                            children: res
                        }
                    ],
                    //selectCategory: res[0]
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                //this.setState({ categories404: true, categoriesLoad: false, })
            })
    }

    showDialogOrderSent = () => {
        const { isRTL } = this.props
        return (
            <Dialog
                width={responsiveWidth(90)}
                onHardwareBackPress={() => { this.setState({ showSentOrderDialog: false }) }}
                visible={this.state.showSentOrderDialog}
            /*onTouchOutside={() => {
                this.setState({ showSentOrderDialog: false });
            }}*/
            >
                <View style={{ width: responsiveWidth(90), marginVertical: moderateScale(10), alignItems: 'center' }}>
                    <Text style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderSentSuccessfuly}</Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ showSentOrderDialog: false })
                            pop()
                        }}
                        style={{ width: responsiveWidth(30), height: responsiveHeight(6), alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(2), marginTop: moderateScale(3) }}>

                        <Text style={{ color: 'white', fontSize: responsiveFontSize(6), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.finish}</Text>
                    </TouchableOpacity>




                </View>
            </Dialog>
        )
    }


    MakeOrder = () => {
        const { name, email, selectCategory, selectImportance, expectedTime, estimatedBudget, orderDescription, image } = this.state
        const { data } = this.props

        var day = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        var currentDate = year + '-' + month + '-' + day
        var currentTime = hours + ':' + min

        if (!name.replace(/\s/g, '').length) {
            this.setState({ name: '', nameUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        /*if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '', emailUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        } else {
            if (InputValidations.validateEmail(email) == false) {
                RNToasty.Error({ title: Strings.emailNotValid })
            }
        }*/
        if (!estimatedBudget.replace(/\s/g, '').length) {
            this.setState({ estimatedBudget: '', })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }

        if (isNaN(estimatedBudget)) {
            this.setState({ estimatedBudgetNumber: '', })
        }

        /*if (isNaN(expectedTime)) {
            this.setState({ expectedTimeNumber: '', })
        }*/
        /*if (!selectCategory) {
            this.setState({ selectCategory:false})
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/
        /*if (!expectedTime.replace(/\s/g, '').length) {
            this.setState({ expectedTime: '', })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/

        /*if (!orderDescription.replace(/\s/g, '').length) {
            this.setState({ orderDescription: '', orderDescriptionUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/

        /*if (!selectImportance) {
            this.setState({ selectImportance: false })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }*/

        if (estimatedBudget.replace(/\s/g, '').length && !isNaN(estimatedBudget) && name.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            var dataSend = new FormData()
            dataSend.append('title', name)
            dataSend.append('details', orderDescription)
            dataSend.append('time', currentTime)
            dataSend.append('date', currentDate)
            dataSend.append('provider_id', data.id)
            //dataSend.append('important', selectImportance)
            //dataSend.append('expected_time', expectedTime)
            dataSend.append('expected_money', this.convertNumbers2English(estimatedBudget))
            dataSend.append('lng', '30')
            dataSend.append('lat', '31')
            if (image) {
                dataSend.append('attachment', {
                    uri: image,
                    type: 'multipart/form-data',
                    name: 'attachment'
                })
            }

            axios.post(`${BASE_END_POINT}client/ordered/send`, dataSend, {
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + this.props.currentUser.data.token
                    //'Content-Type': 'multipart/form-data',
                },
            })
                .then(response => {
                    this.setState({ loading: false })
                    const res = response.data
                    if ('data' in res) {
                        console.log('Done  ', res)
                        //RNToasty.Success({ title: Strings.orderSentSuccessfuly })
                        //Alert.alert(Strings.orderSentSuccessfuly)
                        this.setState({ showSentOrderDialog: true })

                        //this.setState({ currentPage: 4 })
                    } else {
                        if (response.data.status == true) {
                            RNToasty.Success({ title: res.message })
                            //Alert.alert(res.message)
                            // this.setState({ currentPage: 4 })
                        } else {
                            console.log('Error  ', res)
                            RNToasty.Error({ title: res.msg })
                        }
                    }
                })
                .catch(error => {
                    RNToasty.Error({ title: Strings.errorInDataInserting })
                    console.log('Error  ', error)

                    this.setState({ loading: false })

                })
        }
    }

    convertNumbers2English(string) {
        return string.replace(/[\u0660-\u0669]/g, function (c) {
            return c.charCodeAt(0) - 0x0660;
        }).replace(/[\u06f0-\u06f9]/g, function (c) {
            return c.charCodeAt(0) - 0x06f0;
        });
    }

    handleExpectedTime(val) {
        var yas = ''
        yas = (val.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
            return d.charCodeAt(0) - 1632;
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) { return d.charCodeAt(0) - 1776; })
        );
        this.setState({ expectedTime: yas, expectedTimeUpdt: val })
    }

    handleEstimatedBudget(val) {
        var yas = ''
        yas = (val.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
            return d.charCodeAt(0) - 1632;
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) { return d.charCodeAt(0) - 1776; })
        );
        this.setState({ estimatedBudget: yas, estimatedBudgetUpdt: val })
    }



    nameInput = () => {
        const { name, nameUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                {/*<View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.writeYourOrderTitle}</Text>
                </View>*/}

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(4), borderWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(6) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val, nameUpdt: val }) }}
                        placeholder={Strings.writeYourOrderTitle}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'black', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left', padding: 0 }} />
                </View>
                {nameUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    emailInput = () => {
        const { email, emailUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                {/*<View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.email}</Text>
                </View>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val, emailUpdt: val }) }}
                        placeholder={Strings.email}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {emailUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                {/*<View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.phone}</Text>
                </View>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        placeholder={Strings.phone}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    orderAddressInput = () => {
        const { orderAddress, orderAddressUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                {/*<View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderAddress}</Text>
                </View>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ orderAddress: val }) }}
                        placeholder={Strings.createAnAttractivTitle}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*orderAddressUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }

    categoryAndImportancePickers = () => {
        const { isRTL } = this.props
        const { selectedItems, categories, selectCategory, selectImportance } = this.state
        return (
            <View style={{ alignSelf: 'center' }} >

                {/*country */}
                {/*
                <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 0.5, borderBottomColor: '#cccbcb' }}>

                   
                    <View style={{  backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(83), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont }
                            }}
                            items={categories}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectCategory ? this.state.selectCategory.name : strings.selectCategory}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            hideConfirm
                            searchPlaceholderText={Strings.search}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectCategory: selectedItems[0] });
                            }
                            }
                        />
                         {selectCategory==false&&
                         <Text style={{marginTop:moderateScale(5), color:'red',alignSelf:isRTL?'flex-start':'flex-end'}} >{strings.require}</Text>
                        }
                    </View>

                </Animatable.View>
                */}
                {/*city */}
                <Animatable.View style={{ marginTop: moderateScale(9), width: responsiveWidth(85), alignSelf: 'center', borderBottomWidth: 0.5, borderBottomColor: '#cccbcb', }}>

                    {/*<View style={{ marginBottom: moderateScale(2) }}>
                        <Text style={{textAlign:isRTL?'right':'left', color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.selectImportance}</Text>
                    </View>*/}
                    <View style={{ backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>

                        <SectionedMultiSelect
                            styles={{
                                selectToggle: { width: responsiveWidth(83), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center' },
                                selectToggleText: { textAlign: isRTL ? 'right' : 'left', fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont },
                                subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                                container: { minHeight: responsiveHeight(45), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(27), alignSelf: 'center' },
                                button: { backgroundColor: colors.blueTabs }
                            }}
                            items={this.importance}
                            alwaysShowSelectText
                            single
                            uniqueKey="id"
                            subKey="children"
                            selectText={this.state.selectImportance ? this.state.selectImportance : strings.selectImportance}
                            readOnlyHeadings={true}
                            expandDropDowns
                            showDropDowns={false}
                            modalWithTouchable
                            confirmText={strings.close}
                            hideSearch
                            searchPlaceholderText={strings.search}
                            onSelectedItemsChange={(selectedItems) => {
                                // this.setState({ countries: selectedItems });
                            }
                            }
                            onSelectedItemObjectsChange={(selectedItems) => {
                                console.log("ITEM2   ", selectedItems[0].name)
                                this.setState({ selectImportance: selectedItems[0].name });
                            }
                            }
                        />
                        {selectImportance == false &&
                            <Text style={{ marginTop: moderateScale(5), color: 'red', alignSelf: isRTL ? 'flex-start' : 'flex-end' }} >{strings.require}</Text>
                        }
                    </View>

                </Animatable.View>

            </View>
        )
    }

    expectedTimeInput = () => {
        const { expectedTime, expectedTimeUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                {/*<View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.expectedTime}</Text>
                </View>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.handleExpectedTime(val) }}
                        placeholder={Strings.expectedTimeByDay}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {expectedTime.length == 0 ?
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    :
                    isNaN(expectedTime) &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.valueEnteredNumberOnly}</Text>
                }
            </Animatable.View>
        )
    }

    estimatedBudgetInput = () => {
        const { estimatedBudget, estimatedBudgetUpdt, estimatedBudgetNumber } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                {/*<View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.estimatedBudget}</Text>
                </View>*/}
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(4), borderWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(6) }}>
                    <TextInput
                        onChangeText={(val) => { this.handleEstimatedBudget(val) }}
                        placeholder={Strings.estimatedBudgetBySar}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'black', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left', padding: 0 }} />
                </View>
                {estimatedBudget.length == 0 ?
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    :
                    isNaN(estimatedBudget) &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.valueEnteredNumberOnly}</Text>
                }
            </Animatable.View>
        )
    }

    orderDescriptionInput = () => {
        const { orderDescription, orderDescriptionUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(7), width: responsiveWidth(90), alignSelf: 'center' }}>
                {/*<View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderDescription} ({Strings.optional})</Text>
                </View>*/}
                <View style={{ borderColor: colors.lightGray, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, height: responsiveHeight(25), marginTop: moderateScale(5) }}>

                    <TextInput
                        onChangeText={(val) => { this.setState({ orderDescription: val, orderDescriptionUpdt: val }) }}
                        multiline={true}
                        placeholder={Strings.orderDescription + ' (' + Strings.optional + ')'}
                        placeholderTextColor={colors.lightGray}
                        underlineColorAndroid='transparent' style={{ textAlignVertical: 'top', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'black', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>

            </Animatable.View>
        )
    }

    attachmentsInput = () => {
        const { isRTL } = this.props
        const { image, fileName } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), width: responsiveWidth(85), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>

                <Text style={{ marginHorizontal: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left', color: 'black' }}> {Strings.attachmentIfFound}</Text>
                <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(5), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                            }).then(image => {

                                this.setState({ image: image.path, fileName: image.filename })
                            });
                        }}
                        style={{ backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(25), height: responsiveWidth(25) }} >
                        <Icon name='plus' type='AntDesign' style={{ color: colors.black, fontSize: responsiveFontSize(15) }} />
                    </Button>

                    <Text style={{ marginHorizontal: moderateScale(8), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' }}> {fileName}</Text>


                </View>
            </View>
        )
    }


    nextpreviousButton = () => {
        const { name, email, phone, message } = this.state
        const { currentUser, isRTL } = this.props;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(70), marginVertical: moderateScale(10) }}>
                <TouchableOpacity onPress={() => pop()} style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.grayButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.previous}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { this.MakeOrder() }}
                    style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.sendOrder}</Text>
                </TouchableOpacity>
            </View>
        )
    }


    content = () => {
        const { isRTL, barColor } = this.props
        return (
            <View>
                <View style={{ borderRadius: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center', marginTop: moderateScale(10), }}>
                    {this.nameInput()}
                    {/*this.emailInput()*/}
                    {/*this.phoneInput()*/}
                    {/*this.orderAddressInput()*/}
                    {/*this.categoryAndImportancePickers()*/}
                    {/*this.expectedTimeInput()*/}
                    {this.estimatedBudgetInput()}
                </View>
                {this.orderDescriptionInput()}
                {this.attachmentsInput()}
                {this.nextpreviousButton()}
                {this.showDialogOrderSent()}
                {this.state.loading && <LoadingDialogOverlay title={strings.wait} />}
            </View>
        )
    }

    render() {
        const { currentUser, isRTL } = this.props;
        const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.makeOrder} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.makeOrder} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(MakeOrder);