import React,{Component} from 'react';
import {View,TouchableOpacity,TextInput,Text,Modal, Alert,ScrollView,FlatList,ActivityIndicator,RefreshControl} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import {Icon,Thumbnail} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import FastImage from 'react-native-fast-image'
import ListFooter from '../common/ListFooter';
import ChatCard from '../components/ChatCard'
import {removeItem} from '../actions/MenuActions';
import {UploadImage,init} from 'react-native-cloudinary-x'
import {getUnseenMessages,openImage} from '../actions/ChatAction'
import AppHeader from '../common/AppHeader';
import {enableSideMenu,pop} from '../controlls/NavigationControll'
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import NetInfo from "@react-native-community/netinfo";
import {arrabicFont,englishFont} from '../common/AppFont'
import * as colors from '../assets/colors'
import {BASE_END_POINT} from '../AppConfig'
import strings from '../assets/strings'
import CollaspeAppHeader from '../common/CollaspeAppHeader'

class Chat extends Component {

    socket=null;
    index=0;
    page=1;
    flat_list=null;
    state= {
        messages: [],
        messagesLoad:true,
        messages404:false,
        pages:null,
        top:false,
        msg:null,
    }


    constructor(props) {
        super(props);        
        //init('998662858966247','8wKssviRIOCcrcCt7UyObDDbHP0', 'dqho8n6kg')
      
    }

    componentDidMount(){
      console.log(this.props.data)
      enableSideMenu(false, this.props.isRTL)
      this.getMessages(1)
      /*NetInfo.addEventListener(state => {
        if(state.isConnected){
            this.page=1
            this.getMessages(1,false);
            this.setState({loading:true,networkError:false})
        }else{
            this.setState({loading:false,networkError:true})
        }
      });*/
         
    }

    getMessages = (page) => {
      axios.get(`${BASE_END_POINT}inbox/${this.props.data.id}?page=${page}`,{
          headers:{
              Authorization: `Bearer ${this.props.currentUser.data.token}` 
          }
      })
      .then(response=>{
      
        this.setState({
            messages:[...this.state.messages,...response.data.data.chats],
            messagesLoad:false,
            pages:response.data.data.paginate.total_pages,
          })
        })
      .catch(error=>{
       
        this.setState({messagesLoad:false,messages404:true})
      })
  }
    


sendMessage() {
  //console.log(this.props.data.provider_id, this.state.msg,this.props.data.id, this.props.currentUser)
  const data = new FormData()
  //data.append('receiver_id',this.props.data.provider_id)
  data.append('receiver_id',this.props.currentUser.data.id)
  data.append('message',this.state.msg)
  data.append('order_id',this.props.data.id)
  axios.post(`${BASE_END_POINT}send/message`,data, {
      headers: {
        //'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.data.token}`,
      },
    })
      .then(response => {
          this.setState({messages:[...this.state.messages,response.data.data], msg:'',img:null})
         
      }).catch(error => {
         
         
      })  
}

   
    renderFooter = () => {
        return (
          this.state.load ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      renderHeader = () => {
        return (
          this.state.load ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

      
      
    render(){
        const {navigator,isRTL,data,currentUser} = this.props;
        return(
            <View style={{flex:1}}>
               <CollaspeAppHeader back title={strings.chat} />

              <TouchableOpacity 
              onPress={()=>{
              if(this.page <= this.state.pages){
                this.page=this.page+1
                this.getMessages(this.page)
              }
              
              }}
              style={{marginVertical:moderateScale(5),marginBottom:moderateScale(20), backgroundColor:'#616771',borderRadius:moderateScale(3), alignSelf:'center',width:responsiveWidth(25),height:responsiveHeight(5),justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'white'}}>{strings.loadMore}</Text>
              </TouchableOpacity>

                <ScrollView  
                ref={ref => this.scrollView = ref}
                    onContentSizeChange={(contentWidth, contentHeight)=>{        
                    this.scrollView.scrollToEnd({animated: true});
                }}
                 style={{flex:1, marginBottom:moderateScale(28), width:responsiveWidth(100)}}
                 >

                {this.state.messages404?
                <NetworError/>
                :
                this.state.messagesLoad?
                <Loading/>
                :
                this.state.messages.length>0?      
                <FlatList
                
                extraData={this.state}                    
                ref={(ref)=>this.flat_list=ref}
                onContentSizeChange={() => !this.state.top&&this.flat_list.scrollToEnd({animated: true})}
                onLayout={() => !this.state.top&&this.flat_list.scrollToEnd({animated: true})}
                data={this.state.messages}
                renderItem={({item})=> <ChatCard chatRow={item}/> }
                />
                :
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                    <Text style={{ color: colors.darkBlue, marginTop: moderateScale(20), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(7) }}>{strings.noMessagesFound}</Text>
                                </View>
                }

                </ScrollView>

                <View style={{position:'absolute',bottom:0,left:0,right:0, borderWidth:2,borderColor:'white', backgroundColor:'white', elevation:12, alignSelf:'center', width:responsiveWidth(100),height:responsiveHeight(8),flexDirection:'row',alignItems:'center'}}>     
        
                    <TextInput
                    value={this.state.msg}
                    placeholder={strings.writeMessage}
                    placeholderTextColor={colors.darkGray}
                    onChangeText={(val)=>{     
                      this.setState({msg:val})
                    }}
                    underlineColorAndroid='transparent'
                    style={{fontFamily:englishFont, width:responsiveWidth(80),marginHorizontal:moderateScale(7), color:'black'}}
                     />
                    <TouchableOpacity 
                     onPress={()=>{    
                       if(this.state.msg){
                          this.sendMessage()
                        }
                     }}
                    style={{justifyContent:'center',alignItems:'center', backgroundColor:'transparent'}}>
                        <Icon type='MaterialIcons' name='send' style={{color:colors.darkBlue,fontsize:15}} />
                    </TouchableOpacity>
                </View>
                  
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser:state.auth.currentUser, 
    barColor: state.lang.color,
    open:state.chat.open,
    image:state.chat.image,
})

const mapDispatchToProps = {
  removeItem,
  getUnseenMessages,
  openImage
}

export default connect(mapStateToProps,mapDispatchToProps)(Chat);
