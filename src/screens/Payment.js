import React, { Component } from 'react';
import { View, RefreshControl, Alert, FlatList, ScrollView, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import ListFooter from '../common/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import { enableSideMenu, pop, push } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header'
import PageTitleLine from '../components/PageTitleLine'
import WebView from 'react-native-webview'
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class Payment extends Component {

    page = 1;
    list = [];
    state = {
        networkError: null,
        rooms: [],
        refresh: false,
        loading: false,
        pages: null,
        paymentType: 0,
        currentPage: 0,
        sandboxUrl: ''

    }



    componentDidMount() {
        enableSideMenu(false, this.props.isRTL)
        console.log("CURRENT USER  ", this.props.currentUser.data.role)
    }


    componentWillUnmount() {
        this.props.removeItem('PAYMENT')
    }

    paymentAction = () => {

        this.setState({ loading: true })
        var data = new FormData()
        data.append('sub_categories[]', 43)
        axios.post(`${BASE_END_POINT}user/upgrade/paypal`, data, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {
                    console.log('Done  ', res)
                    this.setState({ loading: false, currentPage: 1, sandboxUrl: res.data })
                } else {
                    if (response.data.status == true) {
                        RNToasty.Success({ title: res.message })
                        this.setState({ loading: false, currentPage: 1 })
                    } else {
                        console.log('Error  ', res)
                        RNToasty.Error({ title: res.msg })
                    }
                }
            })
            .catch(error => {

                console.log('Error  ', error)
                this.setState({ loading: false })

            })

    }

    page2() {
        const { sandboxUrl } = this.state
        const { isRTL } = this.props
        return (

            <WebView
                source={{ uri: sandboxUrl }}
                style={{ marginTop: 20, width: responsiveWidth(100), height: responsiveHeight(50) }}
            />

        )
    }

    page1 = () => {
        const { isRTL, barColor } = this.props
        const { paymentType } = this.state

        return (
            <View>

                <PageTitleLine title={Strings.thePay} iconName='money' iconType='FontAwesome' />

                {/*<View style={{ marginTop: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>
                    <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(7), color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.paymentMessage}</Text>
        </View>

                <View style={{ marginTop: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>
                    <Text style={{ fontSize: responsiveFontSize(8), marginHorizontal: moderateScale(7), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.paymentOptions}</Text>
                </View>*/}

                <View style={{ alignSelf: 'center', width: responsiveWidth(80), marginBottom:moderateScale(3) }}>
                    <TouchableOpacity onPress={() => {
                        //this.setState({ paymentType: 1 })
                        push('BankAccounts', { data: Strings.payForExcellence, transferType: 2 })
                    }}
                        style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: colors.white, marginTop: moderateScale(12), borderRadius: moderateScale(5), width: responsiveWidth(80), height: responsiveHeight(8), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <View style={{ height: responsiveHeight(8), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginHorizontal: moderateScale(5) }}>
                            <Icon name='star-o' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.darkBlue }} />
                            <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.payForExcellence}</Text>
                        </View>
                        {paymentType == 1 &&
                            <View style={{ height: responsiveHeight(8), alignItems: 'center', justifyContent: 'center', marginHorizontal: moderateScale(5) }}>
                                <Icon name='checkcircle' type='AntDesign' style={{ color: 'orange' }} />
                            </View>
                        }
                    </TouchableOpacity>

                    {/*<TouchableOpacity onPress={() => 
                        //this.setState({ paymentType: 2 })
                        push('UploadTransferReceipt',{data:Strings.cardDesign})
                        } style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: colors.white, marginTop: moderateScale(8), borderRadius: moderateScale(5), width: responsiveWidth(80), height: responsiveHeight(8), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <View style={{ height: responsiveHeight(8), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginHorizontal: moderateScale(5) }}>
                            <Icon name='squared-plus' type='Entypo' style={{fontSize:responsiveFontSize(7), color: colors.darkBlue }} />
                            <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.cardDesign}</Text>
                        </View>
                        {paymentType == 2 &&
                            <View style={{ height: responsiveHeight(8), alignItems: 'center', justifyContent: 'center', marginHorizontal: moderateScale(5) }}>
                                <Icon name='checkcircle' type='AntDesign' style={{ color: 'orange' }} />
                            </View>
                        }
                    </TouchableOpacity>*/}

                    {this.props.currentUser.data.role == 'provider' &&
                        <TouchableOpacity onPress={() =>// this.setState({ paymentType: 3 })
                            push('BankAccounts', { data: Strings.websiteCommission, transferType: 1 })
                        } style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: colors.white, marginTop: moderateScale(12), borderRadius: moderateScale(5), width: responsiveWidth(80), height: responsiveHeight(8), alignSelf: 'center', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <View style={{ height: responsiveHeight(8), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', marginHorizontal: moderateScale(5) }}>
                                <Icon name='money' type='FontAwesome' style={{ fontSize: responsiveFontSize(7), color: colors.darkBlue }} />
                                <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.websiteCommission}</Text>
                            </View>
                            {paymentType == 3 &&
                                <View style={{ height: responsiveHeight(8), alignItems: 'center', justifyContent: 'center', marginHorizontal: moderateScale(5) }}>
                                    <Icon name='checkcircle' type='AntDesign' style={{ color: 'orange' }} />
                                </View>
                            }
                        </TouchableOpacity>
                    }


                    {/*<TouchableOpacity onPress={()=>{
                         this.paymentAction()
                    }} style={{ elevation: 4, shadowOffset: { height: 0, width: 2 }, shadowOpacity: 0.2, backgroundColor: 'green', marginVertical: moderateScale(15), borderRadius: moderateScale(5), width: responsiveWidth(30), height: responsiveHeight(6.5), alignSelf:isRTL?'flex-start':'flex-end', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: responsiveFontSize(7), marginHorizontal: moderateScale(3), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.pay}</Text>
                    </TouchableOpacity>*/}
                </View>

            </View>
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { paymentType, currentPage, loading } = this.state
        return (
            <>

                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {loading && <LoadingDialogOverlay title={Strings.wait} />}


            </>

        )
    }

    render() {
        const { isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.thePay} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.thePay} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
