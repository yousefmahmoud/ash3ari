import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, FlatList, TouchableOpacity, Text, RefreshControl, TextInput, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { getOrdersCount } from '../actions/OrdersActions'
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors'
import OrderCard from '../components/OrderCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import Dialog, { DialogTitle, DialogContent } from 'react-native-popup-dialog';
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import RNPrint from 'react-native-print';
import { ScrollView } from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'


class Orders extends Component {
    page = 1

    state = {
        //pending
        orders: [],
        ordersLoad: true,
        ordersRefresh: false,
        orders404: null,
        pages: 0,

        dialogOrderData: null,
        showDialog: false,
        reason: 'no needed',
        tabNumber: 1,
    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidMount() {
        const { currentUser } = this.props
        enableSideMenu(false, this.props.isRTL)
        this.getOrders(false, 1, 1)
        this.props.getOrdersCount(currentUser.data.token, currentUser.data.role)
    }

    componentDidUpdate() {
        const { currentUser } = this.props
        enableSideMenu(false, this.props.isRTL)
        this.props.getOrdersCount(currentUser.data.token, currentUser.data.role)
    }

    //pending
    getOrders = (refresh, page, tabNumber) => {
        var url;
        if (tabNumber == 1) {
            url = `${BASE_END_POINT}client/ordered/pending?page=${page}`
            console.log('1')
        } else if (tabNumber == 2) {
            url = `${BASE_END_POINT}client/ordered/accepted?page=${page}`
            console.log('2')
        } else if (tabNumber == 3) {
            url = `${BASE_END_POINT}client/ordered/finished?page=${page}`
            console.log('3')
        } else {
            url = `${BASE_END_POINT}client/ordered/canceled?page=${page}`
            console.log('4')
        }


        if (refresh) {
            this.setState({ ordersRefresh: true })
        }
        axios.get(url, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`
            }
        })
            .then(response => {
                console.log('Done   ', response.data.data.orders)
                this.setState({
                    orders: refresh ? response.data.data.orders : [...this.state.orders, ...response.data.data.orders],
                    ordersLoad: false,
                    ordersRefresh: false,
                    pages: response.data.data.paginate.total_pages,
                })
            })
            .catch(error => {
                console.log('Error   ', error.response)
                this.setState({ orders404: true, ordersLoad: false, })
            })
    }

    cancelOrder = (id) => {
        var data = new FormData()
        data.append('order_id', id)
        //data.append('reason', reason)
        axios.post(`${BASE_END_POINT}client/ordered/cancel`, data, {
            headers: {
                Authorization: `Bearer ${this.props.currentUser.data.token}`,
            }
        })
            .then(response => {
                console.log('CANCEL  ', response)
                this.page = 1
                this.getOrders(true, 1, 1)
                //this.setState({ordersRefresh:true})
            })
            .catch(error => {
                console.log('ERROR  ', error.response)
            })
    }

    item = (key, val, isButton, data) => {
        const { isRTL } = this.props
        return (
            <View style={{ borderBottomWidth: 0, borderBottomColor: '#cccbcb', marginTop: moderateScale(0), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(5), marginVertical: isButton ? moderateScale(5) : 0 }} >
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: colors.sky, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{key} : </Text>
                {isButton ?

                    <View style={{ borderRadius: moderateScale(3), borderColor: colors.lightGray, padding: moderateScale(2) }} onPress={() => { this.setState({ dialogOrderData: data, showDialog: true }) }}>
                        <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: colors.greenApp, textAlign: 'center', marginHorizontal: moderateScale(0) }} >{val}</Text>

                    </View>
                    :
                    <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'gray', textAlign: 'center', marginHorizontal: moderateScale(0) }} >{val}</Text>
                }
            </View>
        )
    }

    orderDetailsItem = (key, val,) => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(6), borderBottomColor: '#cccbcb', flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(90) }} >
                <Text style={{ width: responsiveWidth(25), fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: colors.sky, textAlign: isRTL ? 'right' : 'left', marginHorizontal: moderateScale(4) }} >{key} : </Text>
                <View style={{ width: responsiveWidth(53), borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(2), justifyContent: 'center' }}>
                    <Text style={{ textAlign: isRTL ? 'right' : 'left', fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'gray', marginHorizontal: moderateScale(2) }} >{val}</Text>
                </View>
            </View>
        )
    }

    print = async (data) => {
        var s;
        if (this.state.tabNumber == 1) {
            s = Strings.pending
        } else if (this.state.tabNumber == 2) {
            s = Strings.accepted
        } else if (this.state.tabNumber == 3) {
            s = Strings.finished
        } else {
            s = Strings.canceld
        }
        var recive = `
        <html>
        <head>
            <style>
                .parent{
                    display: flex;
                    flex: 1;
                    flex-direction: column;
                    margin-bottom: 30px;
                }
                .title{
                   border-bottom: 3px solid #16476A;
                    padding: 10px;
                    color: #16476A;
                    align-self: center;
                }
                .item{
                    margin-top: 15px;
                    margin-right: 10px;
                     align-self: flex-end;
                    border-bottom: 1px solid #cccbcb;
                    padding-bottom: 5px;
                    
                    
                }
                .itemKey{
                    font-size: 24px;
                    font-weight: 700;
                   
                   
                   
                }
                .itemVal{
                    font-size: 18px;
                    font-weight: 700;
                    margin-right: 5px;
                    
                }
            </style>
        </head>
        <body>
            <div class="parent" >
                <h1 class="title" >فتورة الطلب </h1>
                
                <div class="item">
                    <span class="itemKey">عنوان الطلب :</span>
                    <span class="itemVal">${data.title}</span>
                </div>
                
                <div class="item">
                    <span class="itemKey">رقم الطلب :</span>
                    <span class="itemVal">${data.id}</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">تاريخ الطلب :</span>
                    <span class="itemVal">${data.full_date}</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">حالة الطلب :</span>
                    <span class="itemVal">${s}</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">المدة المتوقعة :</span>
                    <span class="itemVal">${data.expected_time} يوم</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">السعر :</span>
                    <span class="itemVal">${data.expected_money} ريال سعودى</span>
                </div>
                
                 <div class="item">
                    <span class="itemKey">التفاصيل :</span>
                    <span class="itemVal">${data.details}</span>
                </div>
                
                
                <div class="item">
                <span class="itemKey">اسم العميل :</span>
                <span class="itemVal">${this.props.currentUser.data.name} </span>
                </div>
                
                <div class="item">
                    <span class="itemKey">رقم العميل فى قاعدة البيانات: </span>
                    <span class="itemVal">${this.props.currentUser.data.id}</span>
                </div>
            
                
                <div class="item">
                    <span class="itemKey">اسم مقدم الخدمة :</span>
                    <span class="itemVal">${data.name}</span>
                </div>
            
                <div class="item">
                    <span class="itemKey">رقم مقدم الخدمة فى قاعدة البيانات :</span>
                    <span class="itemVal">${data.provider_id}</span>
                </div>     
             
                
            </div>
        </body>
    </html>`
        await RNPrint.print({
            html: recive
        })
    }

    OrderCardView = (data) => {
        const { isRTL, navigator } = this.props;
        var s;
        if (this.state.tabNumber == 1) {
            s = Strings.pending
        } else if (this.state.tabNumber == 2) {
            s = Strings.accepted
        } else if (this.state.tabNumber == 3) {
            s = Strings.finished
        } else {
            s = Strings.canceld
        }

        return (
            <View >
                <Animatable.View animation={"zoomIn"} duration={1000} style={{ marginVertical: moderateScale(0.1), backgroundColor: colors.white, borderRadius: moderateScale(3), width: responsiveWidth(90), shadowOffset: { height: 2, width: 0 }, borderStyle: 'solid', borderColor: colors.darkBlue, borderWidth: 0.5, shadowColor: 'black', shadowOpacity: 0.1, elevation: 2, marginTop: moderateScale(5), alignSelf: 'center' }}>
                    {this.item(Strings.orderNumber, data.id)}
                    {this.item(Strings.orderDate, data.full_date)}
                    {/*this.item(Strings.importance, data.important)*/}
                    {this.item(Strings.orderStatus, s, true, data)}
                    <TouchableOpacity
                        onPress={() => {
                            //this.setState({ dialogOrderData: data, showDialog: true })
                            push('OrderDetails', data)
                        }}
                        style={{ margin: moderateScale(5), borderRadius: moderateScale(3), height: 40, width: responsiveWidth(25), backgroundColor: colors.sky, alignSelf: isRTL ? 'flex-start' : 'flex-end' }}>
                        <Text style={{ ffontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), lineHeight: 40, color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.details}</Text>
                    </TouchableOpacity>

                    <View style={{ width: responsiveWidth(86), justifyContent: 'space-between', flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(0), alignSelf: 'center' }}>


                        {
                            /* <TouchableOpacity
                                 onPress={() => {
                                     push('Chat', data)
                                 }}
                                 style={{ margin: moderateScale(5), marginTop: moderateScale(-10), borderRadius: moderateScale(3), height: 40, width: responsiveWidth(25), backgroundColor: colors.greenApp, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                                 <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.chat}</Text>
                             </TouchableOpacity>*/
                        }

                        {/*<TouchableOpacity
                        onPress={() => {
                            this.print(data)
                        }}
                        style={{ margin: moderateScale(5), marginTop: moderateScale(-10),  borderRadius: 20, height: 40, width: 40, backgroundColor: colors.sky, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='print' type='FontAwesome' style={{ fontSize: responsiveFontSize(9), color: 'white', }} />
                    </TouchableOpacity>*/}
                    </View>

                </Animatable.View>
            </View>
        )
    }

    orderDetailsDialog = () => {
        const { isRTL, navigator, data } = this.props;
        const { dialogOrderData } = this.state
        return (
            <Dialog
                visible={this.state.showDialog}
                onTouchOutside={() => this.setState({ showDialog: false })}
                onHardwareBackPress={() => this.setState({ showDialog: false })}
            //dialogTitle={<DialogTitle title="Dialog Title" />}
            >
                <View style={{ width: responsiveWidth(90), height: responsiveHeight(85) }}>
                    <View style={{ backgroundColor: colors.darkBlue, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(90), height: responsiveHeight(7) }}>
                        <Text style={{ fontFamily: englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(0) }} >{Strings.orderDetails}</Text>
                    </View>
                    <ScrollView >
                        {this.orderDetailsItem(Strings.name, dialogOrderData.provider.name)}
                        {this.orderDetailsItem(Strings.phone, dialogOrderData.provider.phone)}
                        {this.orderDetailsItem(Strings.orderTitle, dialogOrderData.title)}
                        {this.orderDetailsItem(Strings.orderNumber, dialogOrderData.id)}
                        {this.orderDetailsItem(Strings.orderDate, dialogOrderData.full_date)}
                        {this.orderDetailsItem(Strings.orderStatus, dialogOrderData.status)}
                        {/*this.orderDetailsItem(Strings.category,dialogOrderData.a)*/}
                        {/*this.orderDetailsItem(Strings.expectedTime, dialogOrderData.expected_time)*/}
                        {this.orderDetailsItem(Strings.sallry, dialogOrderData.expected_money)}
                        {this.orderDetailsItem(Strings.details, dialogOrderData.details)}
                    </ScrollView>


                    {dialogOrderData.status == 'pending' &&
                        <View style={{ marginVertical: moderateScale(6), justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>


                            <Button
                                style={{ marginTop: moderateScale(0), height: responsiveHeight(6), width: responsiveWidth(25), borderRadius: moderateScale(3), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue }}
                                onPress={() => {
                                    this.setState({ showDialog: false })
                                    this.cancelOrder(dialogOrderData.id)
                                }}>
                                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: 'white', textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.cancel}</Text>
                            </Button>
                            {/*<Button style={{height:responsiveHeight(6), width:responsiveWidth(25),borderRadius:moderateScale(3), justifyContent:'center',alignItems:'center',backgroundColor:colors.sky}} onPress={()=>{this.setState({showDialog:false})}}>
                        <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:'white',textAlign:'center',marginHorizontal:moderateScale(6)}} >{Strings.refuse}</Text> 
                    </Button>
                    */}
                        </View>
                    }

                </View>
            </Dialog>
        )
    }

    tabs = () => {
        const { isRTL } = this.props
        const { tabNumber } = this.state
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', marginTop: moderateScale(6), width: responsiveWidth(90), height: responsiveHeight(7), alignSelf: 'center' }}>
                <TouchableOpacity
                    style={{ height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', borderBottomColor: tabNumber == 1 ? colors.sky : 'white', borderBottomWidth: 2 }}
                    onPress={() => {
                        this.setState({ tabNumber: 1 })
                        this.page = 1
                        this.getOrders(true, 1, 1)
                    }}
                >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: tabNumber == 1 ? colors.darkGray : colors.lightGray, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.new}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', borderBottomColor: tabNumber == 2 ? colors.sky : 'white', borderBottomWidth: 2 }}
                    onPress={() => {
                        this.setState({ tabNumber: 2 })
                        this.page = 1
                        this.getOrders(true, 1, 2)
                    }}
                >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: tabNumber == 2 ? colors.darkGray : colors.lightGray, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.underProcessing}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', borderBottomColor: tabNumber == 3 ? colors.sky : 'white', borderBottomWidth: 2 }}
                    onPress={() => {
                        this.setState({ tabNumber: 3 })
                        this.page = 1
                        this.getOrders(true, 1, 3)
                    }}
                >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: tabNumber == 3 ? colors.darkGray : colors.lightGray, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.finished}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', borderBottomColor: tabNumber == 4 ? colors.sky : 'white', borderBottomWidth: 2 }}
                    onPress={() => {
                        this.setState({ tabNumber: 4 })
                        this.page = 1
                        this.getOrders(true, 1, 4)
                    }}
                >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(6), color: tabNumber == 4 ? colors.darkGray : colors.lightGray, textAlign: 'center', marginHorizontal: moderateScale(6) }} >{Strings.canceld}</Text>
                </TouchableOpacity>

            </View>
        )
    }

    content = () => {
        const { isRTL } = this.props
        const { dialogOrderData, orders, orders404, ordersLoad, ordersRefresh, pages } = this.state
        return (
            <View>
                <PageTitleLine title={Strings.myOrders} iconName='user-alt' iconType='FontAwesome5' />
                {this.tabs()}
                {
                    orders404 ?
                        <NetworError />
                        :
                        ordersLoad ?
                            <Loading />
                            :
                            orders.length > 0 ?
                                <FlatList
                                    style={{ marginBottom: moderateScale(10) }}
                                    data={orders}
                                    renderItem={({ item }) => this.OrderCardView(item)}
                                    onEndReachedThreshold={.5}
                                    //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
                                    onEndReached={() => {
                                        if (this.page <= pages) {
                                            this.page = this.page + 1;
                                            this.getOrders(false, this.page, this.state.tabNumber)
                                            console.log('page  ', this.page)
                                        }
                                    }}
                                    refreshControl={
                                        <RefreshControl
                                            colors={["#B7ED03", colors.darkBlue]}
                                            refreshing={ordersRefresh}
                                            onRefresh={() => {
                                                this.page = 1
                                                this.getOrders(true, 1, this.state.tabNumber)
                                            }}
                                        />
                                    }

                                />
                                :
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                    <Text style={{ color: colors.darkBlue, marginTop: moderateScale(20), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(7) }}>{Strings.noOrders}</Text>
                                </View>
                }
                {dialogOrderData && this.orderDetailsDialog()}

            </View>
        )
    }


    render() {
        const { currentUser, isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={strings.myOrders} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={strings.myOrders} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
        );
    }
}



const mapDispatchToProps = {
    getUser,
    removeItem,
    getOrdersCount
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})


export default connect(mapToStateProps, mapDispatchToProps)(Orders);

