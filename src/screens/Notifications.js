import React,{Component} from 'react';
import {View,RefreshControl,Alert,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../common/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import {selectMenu,removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import {enableSideMenu,pop} from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import {arrabicFont,englishFont} from  '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import { getUnreadNotificationsNumers } from '../actions/NotificationAction';
import ParallaxScrollView from 'react-native-parallax-scroll-view';


class Notifications extends Component {

    page=1;
    state= {
        notifications:[],
        notificationsLoad:true,
        notificationsRefresh:false,
        notifications404:null, 
        pages:0,
    }

    componentDidMount(){
       
        enableSideMenu(false, this.props.isRTL)
        /*NetInfo.addEventListener(state => {
            if(state.isConnected){
                this.page=1
                this.getRooms(1,false);
                this.setState({loading:true,networkError:false})
            }else{
                this.setState({loading:false,networkError:true})
            }
          }); */ 
          this.getNotifications(false,this.page)
          this.props.getUnreadNotificationsNumers(this.props.currentUser.data.token)

    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    componentDidUpdate(){
        enableSideMenu(false, this.props.isRTL)
    }  

    getNotifications = (refresh,page) => {
        if(refresh){
            this.setState({notificationsRefresh:true})
        }
        var uri=''
        if(this.props.currentUser.data.role=='client'){
            uri=`${BASE_END_POINT}client/ordered/notifications?page=${page}`
        }else{
           // Alert.alert("p")
            uri=`${BASE_END_POINT}provider/ordered/notifications?page=${page}`
        }
        axios.get(uri,{
            headers:{
                Authorization: `Bearer ${this.props.currentUser.data.token}` 
            }
        })
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              notifications:refresh?response.data.data.notifications:[...this.state.notifications,...response.data.data.notifications],
              notificationsLoad:false,
              notificationsRefresh:false,
              pages:response.data.data.paginate.total_pages,
            })
          })
        .catch(error=>{
          console.log('Error   ',error)
          this.setState({notifications404:true,notificationsLoad:false,})
        })
    }




    content = () => {
        const {isRTL} = this.props
        const {notifications,notifications404,notificationsLoad,pages,notificationsRefresh} = this.state
         return(
           <View>
            <PageTitleLine title={Strings.notifications} iconName='bell' iconType='FontAwesome' />
            {
            notifications404?
            <NetworError />
            :
            notificationsLoad?
            <Loading />
            :
            notifications.length>0?
            <FlatList 
            
            style={{marginBottom:moderateScale(10), marginTop:moderateScale(4)}}
            data={notifications}
            renderItem={({item})=><NotificationCard data={item}/>}
            onEndReachedThreshold={.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {     
                if(this.page <= pages){
                    this.page=this.page+1;
                    this.getNotifications(false,this.page)
                    console.log('page  ',this.page)
                }  
            }}
            refreshControl={
            <RefreshControl 
            colors={["#B7ED03",colors.darkBlue]} 
            refreshing={notificationsRefresh}
            onRefresh={() => {
                this.page = 1
                this.getNotifications(true,1)
            }}
            />
            }
            
            />
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
            <Text style={{ color: colors.darkBlue, marginTop: moderateScale(20), alignSelf: 'center', fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center', fontSize: responsiveFontSize(7) }}>{Strings.notNotificatios}</Text>
        </View>
            }
          </View>
         )
       }


    render(){
        const {categoryName,isRTL} = this.props;
        return(
            /*<ReactNativeParallaxHeader
             scrollViewProps={{showsVerticalScrollIndicator:false}}
             headerMinHeight={responsiveHeight(10)}
             headerMaxHeight={responsiveHeight(35)}
             extraScrollHeight={20}
             navbarColor={colors.darkBlue}
             backgroundImage={require('../assets/imgs/header.png')}
             backgroundImageScale={1.2}
             renderNavBar={()=><CollaspeAppHeader back title={Strings.notifications} />}
             renderContent={()=>this.content()}
             containerStyle={{flex:1}}
             contentContainerStyle={{flexGrow: 1}}
             innerContainerStyle={{flex: 1,}}
           />*/


           <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.notifications} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>
           
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
    getUnreadNotificationsNumers,
}

export default connect(mapStateToProps,mapDispatchToProps)(Notifications);
