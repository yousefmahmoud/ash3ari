import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';
import * as colors from '../assets/colors'
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import LottieView from 'lottie-react-native';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../common/ListFooter';
import DietCard from '../components/DietCard';
import {removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import {enableSideMenu,push,resetTo} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'


class Diet extends Component {

    page=1;
    state= {
        errorText:null,
        allCategories: new DataProvider(),
        refresh:false,
        loading:true,
        pages:null,
        selectedDay:1,
    }

   
      
    componentWillUnmount(){
        this.props.removeItem()
    }

    componentDidUpdate(){
        enableSideMenu(true,this.props.isRTL)
    }

    constructor(props) {
        super(props);  
        
/*
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
               // this.getAdds(this.page,false);
            }else{
                this.setState({errorText:'Strings.noConnection'})
            }
          });
          */
      }

    componentDidMount(){
        enableSideMenu(true,this.props.isRTL)
        /*
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                   // this.getAdds(this.props.companyID,this.page,true);
                }
            }
          );  
        */    
    }



    getAdds(page, refresh) {
            //this.setState({loading:true})
            let uri = `${BASE_END_POINT}ads?page=${page}&limit=20`
            if (refresh) {
                this.setState({loading: false, refresh: true})
            } else {
                this.setState({refresh:false,loading:true})
            }
            axios.get(uri)
                .then(response => {
                    this.setState({
                        loading:false,
                        refresh:false,
                        pages:response.data.pageCount,
                        errorText:null,
                        allCategories: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.allCategories.getAllData(), ...response.data.data]),
                    })
                    if(response.data.data.length==0){
                        RNToasty.Info({title:Strings.notResults})
                    }
                }).catch(error => {
                    this.setState({loading:false,refresh:false})
                
                    if (!error.response) {
                        this.setState({errorText:Strings.noConnection})
                    }
                })
        
    }

    
    renderFooter = () => {
        return (
          this.state.loading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter />
            </View>
            : null
        )
      }

    render(){
        const {navigator,isRTL} = this.props;
        const {selectedDay} = this.state;
        return(
            <View style={{flex:1}}>
                <AppHeader menu  navigator={navigator} title={Strings.diet}  /> 

                    <DietCard />
                    <DietCard />

                    
                 <View style={{width:responsiveWidth(100),height:responsiveHeight(9),position:'absolute',bottom:0,right:0,left:0,borderTopLeftRadius:moderateScale(8),borderTopRightRadius:moderateScale(8),backgroundColor:'#679C8A',elevation:15,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:1})}
                     style={{backgroundColor:selectedDay==1?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{ fontFamily:englishFont,color:selectedDay==1?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >1</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:2})}
                     style={{backgroundColor:selectedDay==2?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==2?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >2</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:3})}
                     style={{backgroundColor:selectedDay==3?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==3?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >3</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:4})}
                     style={{backgroundColor:selectedDay==4?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==4?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >4</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:5})}
                     style={{backgroundColor:selectedDay==5?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==5?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >5</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:6})}
                     style={{backgroundColor:selectedDay==6?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==6?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >6</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                     onPress={()=>this.setState({selectedDay:7})}
                     style={{backgroundColor:selectedDay==7?'white':'transparent',justifyContent:'center',alignItems:'center',width:20,height:20,borderRadius:10}}>
                        <Text style={{fontFamily:englishFont, color:selectedDay==7?'#207cd0':'white',fontSize:responsiveFontSize(7)}} >7</Text>
                    </TouchableOpacity>

                 </View>
                {/*this.state.loading?
                <View style={{ flex:1}}> 
                    <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                     <LottieView
                    style={{width:responsiveWidth(100),height:responsiveHeight(33.3)}}
                    source={require('../assets/animations/smartGarbageLoading.json')}
                    autoPlay
                    loop
                    />
                </View>
                :
                <RecyclerListView            
                layoutProvider={this.renderLayoutProvider}
                dataProvider={this.state.allCategories}
                rowRenderer={this.renderRow}
                renderFooter={this.renderFooter}
                onEndReached={() => {
                   
                    if(this.page <= this.props.pages){
                        this.page++;
                        this.getAdds(this.page, false);
                    }
                    
                  }}
                  refreshControl={<RefreshControl colors={["#B7ED03"]}
                    refreshing={this.state.refresh}
                    onRefresh={() => {
                      this.page = 1
                      this.getAdds(this.page, true);
                    }
                    } />}
                  onEndReachedThreshold={.5}
        
            />
                */}
                
            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color 
})

const mapDispatchToProps = {
    removeItem
}

export default connect(mapStateToProps,mapDispatchToProps)(Diet);
