import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  StyleSheet,
  Dimensions,
  Keyboard,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView,
  NetInfo,
  TouchableOpacity,
  Platform,
  Alert,
  TextInput,
  ActivityIndicator,
  Text,
} from 'react-native';
import { Button, Item, Label, Icon } from 'native-base';
import { Field, reduxForm, change as changeFieldValue } from 'redux-form';
import { connect } from 'react-redux';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import allStrings from '../assets/strings';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import StepIndicator from 'react-native-step-indicator';
import Swiper from 'react-native-swiper';
import { API_ENDPOINT } from '../AppConfig';
import { arrabicFont, englishFont } from '../common/AppFont';
const { width, height } = Dimensions.get('window');
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import CodeInput from 'react-native-confirmation-code-input';
import { RNToasty } from 'react-native-toasty';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import SnackBar from 'react-native-snackbar-component';
import AppHeader from '../common/AppHeader';
import CountDown from 'react-native-countdown-component';
import { resetTo } from '../controlls/NavigationControll';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import strings from '../assets/strings';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'

titles = [
  allStrings.forgetPassword,
  allStrings.verifyEmail,
  allStrings.newPassword,
];

class ForgetPassword extends Component {
  countryKeys = [
    // this is the parent or 'item'
    {
      name: Strings.countryKey,
      id: 0,
      // these are the children or 'sub items'
      children: [
        {
          name: '966',
          id: 966,
        },
        {
          name: '971',
          id: 971,
        },
        {
          name: '965',
          id: 965,
        },
        {
          name: '973',
          id: 973,
        },
        {
          name: '968',
          id: 968,
        },
      ],
    },
  ];

  static navigatorStyle = {
    navBarHidden: true,
    statusBarColor: '#A1C7C1',
  };

  state = {
    selectCountryKey: '966',
    currentPage: 0,
    phone: ' ',
    password: ' ',
    confirmPassword: ' ',
    loading: false,
    isFullcode: false,
    verifycode: ' ',
    hidePassword: true,
  };
  swiper = null;

  sendCode(code) {

    let body = {
      email: this.state.email.toLowerCase(),
      verifycode: code,
    };
    this.setState({ loading: true });
    axios
      .post(`${BASE_END_POINT}confirm-code`, JSON.stringify(body), {
        headers: { 'Content-Type': 'application/json' },
      })
      .then((response) => {
        this.setState({
          currentPage: 2,
          loading: false,
        });
        //this.swiper.scrollBy(1);
      })
      .catch((error) => {
        this.setState({ loading: false });
        if (error.request.status === 400) {
          RNToasty.Error({ title: allStrings.correctCode });
        }
        if (!error.response) {
          this.setState({ noConnection: 'allStrings.noConnection' });
        }
        console.log('code error');
        console.log(error);
        console.log(error.response);
      });
  }
  _onFulfill(code) {
    this.sendCode(code);
  }

  forgetPassword() {
    const { phone, selectCountryKey } = this.state;
    if (!phone.replace(/\s/g, '').length) {
      this.setState({ phone: '' });
    } else if (isNaN(phone)) {
      RNToasty.Error({ title: allStrings.phoneNotValid });
    } else {
      // let body = { email: this.state.email.toLowerCase(), newPassword: this.state.password }
      var data = new FormData();
      data.append('code', selectCountryKey);
      data.append('phone', phone);
      this.setState({ loading: true });
      axios
        .post(`${BASE_END_POINT}auth/forget`, data, {
          headers: { 'Content-Type': 'application/json' },
        })
        .then((response) => {
          this.setState({ loading: false });
          console.log('Done : ', response.data.status);
          const res = response;
          if ('data' in res) {
            console.log('Done  ', res);
            //  this.setState({ currentPage: 1, loading: false })
            //} else {
            if (response.data.status == true) {
              // RNToasty.Success({ title: strings.codeHse  })
              this.setState({ currentPage: 1, loading: false });
            } else {
              console.log('Error  ', res);
              RNToasty.Error({ title: strings.incorrectNumber });
              this.setState({ loading: false });
            }
          }

          //this.props.navigator.pop()
        })
        .catch((error) => {
          this.setState({ loading: false });
          if (!error.response) {
            this.setState({ noConnection: 'allStrings.noConnection' });
          }
          console.log('new Password error');
          console.log(error);
          console.log(error.response);
        });
    }
  }

  resetPassword() {
    //console.log('rrrrrr');
    const { phone, password, confirmPassword, verifycode, selectCountryKey } = this.state;

    console.log('phone', phone);
    console.log('password', password);
    console.log('confirmPassword', confirmPassword);
    console.log('Reset_code', verifycode);
    console.log('code', selectCountryKey)
    if (!phone.replace(/\s/g, '').length) {
      this.setState({ phone: '' });
    }
    if (!password.replace(/\s/g, '').length) {
      this.setState({ password: '' });
    }
    if (!confirmPassword.replace(/\s/g, '').length) {
      this.setState({ confirmPassword: '' });
    }
    if (!verifycode.replace(/\s/g, '').length) {
      this.setState({ verifycode: '' });
    }
    if (!(selectCountryKey).replace(/\s/g, '').length) {
      this.setState({ code: '' });
    }
    if (
      phone.replace(/\s/g, '').length &&
      password.replace(/\s/g, '').length &&
      confirmPassword.replace(/\s/g, '').length &&
      verifycode.replace(/\s/g, '').length &&
      selectCountryKey.replace(/\s/g, '').length
    ) {
      // let body = { email: this.state.email.toLowerCase(), newPassword: this.state.password }

      var data = new FormData();
      data.append('phone', phone);
      data.append('password', password);
      data.append('password_confirmation', confirmPassword);
      data.append('reset_code', verifycode);
      data.append('code', selectCountryKey);
      this.setState({ loading: true });
      axios
        .post(`${BASE_END_POINT}auth/reset`, data, {
          headers: { 'Content-Type': 'multipart/form-data' },
        })
        .then((response) => {
          this.setState({ loading: false });
          console.log('Done : ', response);
          const res = response.data;
          if ('data' in res) {
            console.log('Done  ', res);
            RNToasty.Success({ title: allStrings.forgetPasswordDone });
            this.setState({ currentPage: 1, loading: false });
            resetTo('Login');
          } else {
            RNToasty.Error({ title: res.msg });
            this.setState({ loading: false });
          }
          //this.props.navigator.pop()
        })
        .catch((error) => {
          this.setState({ loading: false });
          if (!error.response) {
            this.setState({ noConnection: 'allStrings.noConnection' });
          }
          console.log('new Password error');
          console.log(error);
          console.log(error.response);
        });
    }
  }

  renderInsertPhonePage = () => {
    const { phone } = this.state;
    const { isRTL } = this.props;
    return (
      <Animatable.View
        animation={'slideInLeft'}
        duration={1000}
        style={{
          marginTop: moderateScale(10),
          width: responsiveWidth(90),
          alignSelf: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: isRTL ? 'row-reverse' : 'row',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: responsiveWidth(90),
          }}>
          {this.countryKeyInput()}

          <View
            style={{
              flexDirection: isRTL ? 'row-reverse' : 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              width: responsiveWidth(62),
              borderRadius: moderateScale(2),
              borderWidth: 0.5,
              borderColor: colors.darkBlue,
              height: responsiveHeight(7),
            }}>
            <View style={{ marginHorizontal: moderateScale(5) }}>
              <Icon
                name="phone"
                type="FontAwesome"
                style={{
                  color: colors.mediumBlue,
                  fontSize: responsiveFontSize(8),
                }}
              />
            </View>
            <TextInput
              onChangeText={(val) => {
                this.setState({ phone: val });
              }}
              placeholder={allStrings.enterYourPhone}
              underlineColorAndroid="transparent"
              style={{
                fontSize: responsiveFontSize(6),
                fontFamily: isRTL ? arrabicFont : englishFont,
                width: responsiveWidth(48),
                color: 'gray',
                direction: isRTL ? 'rtl' : 'ltr',
                textAlign: isRTL ? 'right' : 'left',
              }}
            />
          </View>
        </View>

        {phone.length == 0 && (
          <Text
            style={{
              color: 'red',
              fontSize: responsiveFontSize(6),
              alignSelf: isRTL ? 'flex-start' : 'flex-end',
            }}>
            {' '}
            {Strings.require}
          </Text>
        )}

        <TouchableOpacity
          onPress={() => {
            this.forgetPassword();
          }}
          style={{
            padding: 0,
            marginHorizontal: moderateScale(10),
            marginTop: moderateScale(40),
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            width: responsiveWidth(60),
            height: responsiveHeight(7),
            borderRadius: moderateScale(1),
            backgroundColor: colors.darkBlue,
          }}>
          <Text
            style={{
              color: colors.white,
              fontFamily: isRTL ? arrabicFont : englishFont,
            }}>
            {Strings.send}
          </Text>
        </TouchableOpacity>
      </Animatable.View>
    );
  };

  renderVerifyCodePage = () => {
    const { navigator, isRTL } = this.props;
    const { code, isFullcode } = this.state;
    return (
      <Animatable.View
        animation={'slideInLeft'}
        duration={1000}
        style={{ width: responsiveWidth(100) }}>
        <View style={{ flex: 1, backgroundColor: 'white' }}>
          <Text
            style={{
              marginHorizontal: moderateScale(5),
              marginTop: moderateScale(5),
              color: '#828282',
            }}>
            {allStrings.enterPin}
          </Text>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              marginTop: moderateScale(20),
              height: responsiveHeight(15),
            }}>
            <CodeInput
              ref="codeInputRef1"
              secureTextEntry
              //compareWithCode='1234'
              codeLength={4}
              className={'border-b'}
              space={5}
              size={30}
              inputPosition="center"
              activeColor="black" //'rgba(49, 180, 4, 1)'
              inactiveColor="#C1C1C1"
              onFulfill={(code) => {
                console.log('code is ', code);
                this.setState({ isFullcode: true, code: code });
              }}
            />
          </View>

          <TouchableOpacity
            style={{
              width: responsiveWidth(90),
              alignSelf: 'center',
              height: responsiveHeight(8),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#679C8A',
              marginTop: moderateScale(12),
              borderRadius: moderateScale(2),
            }}
            onPress={() => {
              if (this.state.isFullcode) {
                //this.sendCode(this.state.code)
                this.setState({
                  currentPage: 2,
                  loading: false,
                });
                // this.swiper.scrollBy(1);
              } else {
                RNToasty.Info({ title: allStrings.pinCode });
              }
            }}>
            <Text style={{ color: 'white', fontSize: responsiveFontSize(8) }}>
              {allStrings.verify}
            </Text>
          </TouchableOpacity>

          <View
            style={{
              marginTop: moderateScale(2),
              width: responsiveWidth(90),
              alignSelf: 'center',
              justifyContent: 'space-between',
              flexDirection: isRTL ? 'row-reverse' : 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                const body = {
                  email: this.state.email.toLowerCase(),
                };
                this.setState({ loading: true });
                axios
                  .post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                    headers: { 'Content-Type': 'application/json' },
                  })
                  .then((response) => {
                    console.log('phone done');
                    console.log(response.data);
                    this.setState({ loading: false });
                  })
                  .catch((error) => {
                    this.setState({ loading: false });

                    console.log('phone error');
                    console.log(error);
                    console.log(error.response);
                  });
              }}>
              <Text style={{ color: '#A3A3A3' }}>{allStrings.resenCode}</Text>
            </TouchableOpacity>
            <CountDown
              until={60 * 2 + 30}
              size={responsiveFontSize(7)}
              onFinish={() => {
                const body = {
                  email: this.state.email.toLowerCase(),
                };
                this.setState({ loading: true });
                axios
                  .post(`${BASE_END_POINT}sendCode`, JSON.stringify(body), {
                    headers: { 'Content-Type': 'application/json' },
                  })
                  .then((response) => {
                    console.log('phone done');
                    console.log(response.data);
                    this.setState({ loading: false });
                  })
                  .catch((error) => {
                    this.setState({ loading: false });

                    console.log('phone error');
                    console.log(error);
                    console.log(error.response);
                  });
              }}
              digitStyle={{ backgroundColor: 'transparent' }}
              digitTxtStyle={{ color: '#A3A3A3' }}
              timeToShow={['M', 'S']}
              timeLabels={{ m: '', s: '' }}
            />
          </View>
        </View>
      </Animatable.View>
    );
  };

  renderInsertPasswordlPage = () => {
    const { navigator, isRTL } = this.props;
    const { password, confirmPassword, verifycode } = this.state;
    return (
      <Animatable.View
        animation={'slideInLeft'}
        duration={1000}
        style={{ width: responsiveWidth(100) }}>
        <View
          style={{
            marginVertical: moderateScale(4),
            width: responsiveWidth(90),
            alignSelf: 'center',
          }}>
          <View
            style={{
              flexDirection: isRTL ? 'row-reverse' : 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              marginTop: moderateScale(3),
              width: responsiveWidth(90),
              borderRadius: moderateScale(2),
              borderWidth: 0.5,
              borderColor: colors.darkBlue,
              height: responsiveHeight(8),
            }}>
            <View style={{ marginHorizontal: moderateScale(5) }}>
              <Icon
                name="note"
                type="Octicons"
                style={{
                  color: colors.mediumBlue,
                  fontSize: responsiveFontSize(8),
                }}
              />
            </View>
            <TextInput
              onChangeText={(val) => {
                this.setState({ verifycode: val });
              }}
              placeholder={allStrings.verifycode}
              underlineColorAndroid="transparent"
              style={{
                fontSize: responsiveFontSize(6),
                fontFamily: isRTL ? arrabicFont : englishFont,
                width: responsiveWidth(72),
                color: 'gray',
                direction: isRTL ? 'rtl' : 'ltr',
                textAlign: isRTL ? 'right' : 'left',
              }}
            />
          </View>
          {verifycode.length == 0 && (
            <Text
              style={{
                color: 'red',
                fontSize: responsiveFontSize(6),
                alignSelf: isRTL ? 'flex-start' : 'flex-end',
              }}>
              {' '}
              {Strings.require}
            </Text>
          )}

          <View
            style={{
              flexDirection: isRTL ? 'row-reverse' : 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              marginTop: moderateScale(3),
              width: responsiveWidth(90),
              borderRadius: moderateScale(2),
              borderWidth: 0.5,
              borderColor: colors.darkBlue,
              height: responsiveHeight(8),
            }}>
            <View style={{ marginHorizontal: moderateScale(5) }}>
              <Icon
                name="md-lock"
                type="Ionicons"
                style={{
                  color: colors.mediumBlue,
                  fontSize: responsiveFontSize(8),
                }}
              />
            </View>
            <TextInput
              secureTextEntry={this.state.hidePassword}
              onChangeText={(val) => {
                this.setState({ password: val });
              }}
              placeholder={allStrings.newPassword}
              underlineColorAndroid="transparent"
              style={{
                fontSize: responsiveFontSize(6),
                fontFamily: isRTL ? arrabicFont : englishFont,
                width: responsiveWidth(72),
                color: 'gray',
                direction: isRTL ? 'rtl' : 'ltr',
                textAlign: isRTL ? 'right' : 'left',
              }}
            />
            <TouchableOpacity
              style={{ marginHorizontal: moderateScale(0) }}
              onPress={() => {
                this.setState({ hidePassword: !this.state.hidePassword });
              }}>
              <Icon
                style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }}
                name={this.state.hidePassword ? 'eye-off' : 'eye'}
                type="Feather"
              />
            </TouchableOpacity>
          </View>
          {password.length == 0 && (
            <Text
              style={{
                color: 'red',
                fontSize: responsiveFontSize(6),
                alignSelf: isRTL ? 'flex-start' : 'flex-end',
              }}>
              {' '}
              {Strings.require}
            </Text>
          )}

          <View
            style={{
              flexDirection: isRTL ? 'row-reverse' : 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              marginTop: moderateScale(3),
              width: responsiveWidth(90),
              borderRadius: moderateScale(2),
              borderWidth: 0.5,
              borderColor: colors.darkBlue,
              height: responsiveHeight(8),
            }}>
            <View style={{ marginHorizontal: moderateScale(5) }}>
              <Icon
                name="md-lock"
                type="Ionicons"
                style={{
                  color: colors.mediumBlue,
                  fontSize: responsiveFontSize(8),
                }}
              />
            </View>
            <TextInput
              secureTextEntry
              onChangeText={(val) => {
                this.setState({ confirmPassword: val });
              }}
              placeholder={allStrings.confirmNewPassword}
              underlineColorAndroid="transparent"
              style={{
                fontSize: responsiveFontSize(6),
                fontFamily: isRTL ? arrabicFont : englishFont,
                width: responsiveWidth(78),
                color: 'gray',
                direction: isRTL ? 'rtl' : 'ltr',
                textAlign: isRTL ? 'right' : 'left',
              }}
            />
          </View>
          {confirmPassword.length == 0 && (
            <Text
              style={{
                color: 'red',
                fontSize: responsiveFontSize(6),
                alignSelf: isRTL ? 'flex-start' : 'flex-end',
              }}>
              {' '}
              {Strings.require}
            </Text>
          )}
        </View>

        <Button
          onPress={() => {
            this.resetPassword();
          }}
          style={{
            padding: 0,
            marginHorizontal: moderateScale(10),
            marginTop: moderateScale(15),
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            width: responsiveWidth(60),
            height: responsiveHeight(7),
            borderRadius: moderateScale(1),
            backgroundColor: colors.darkBlue,
          }}>
          <Text
            style={{
              color: colors.white,
              fontFamily: isRTL ? arrabicFont : englishFont,
            }}>
            {allStrings.resetPassword}
          </Text>
        </Button>
      </Animatable.View>
    );
  };

  countryKeyInput = () => {
    const { countryKey, selectCountryKey } = this.state;
    const { isRTL } = this.props;
    return (
      <Animatable.View
        style={{
          marginTop: moderateScale(1),
          width: responsiveWidth(25),
          alignSelf: 'center',
        }}>
        <View
          style={{
            flexDirection: isRTL ? 'row-reverse' : 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            width: responsiveWidth(25),
            borderRadius: moderateScale(2),
            borderWidth: 0.5,
            borderColor: colors.darkBlue,
            height: responsiveHeight(7),
          }}>
          <View style={{ marginHorizontal: moderateScale(5) }}>
            <Icon
              name="telephone-accessible"
              type="Foundation"
              style={{
                color: colors.mediumBlue,
                fontSize: responsiveFontSize(8),
              }}
            />
          </View>
          <SectionedMultiSelect
            styles={{
              selectToggle: {
                width: responsiveWidth(78),
                height: responsiveHeight(6),
                borderRadius: moderateScale(3),
                alignItems: 'center',
                justifyContent: 'center',
                color: 'white',
              },
              selectToggleText: {
                fontSize: responsiveFontSize(6),
                color: 'gray',
                marginTop: moderateScale(6),
                fontFamily: isRTL ? arrabicFont : englishFont,
                textAlign: isRTL ? 'right' : 'left',
              },
              subItemText: {
                textAlign: 'center',
                fontFamily: isRTL ? arrabicFont : englishFont,
              },
              itemText: {
                fontSize: responsiveFontSize(10),
                textAlign: 'center',
                fontFamily: isRTL ? arrabicFont : englishFont,
              },
              container: {
                height: responsiveHeight(40),
                position: 'absolute',
                width: responsiveWidth(80),
                top: responsiveHeight(22),
                alignSelf: 'center',
              },
              button: { backgroundColor: colors.blueTabs },
            }}
            items={this.countryKeys}
            alwaysShowSelectText
            single
            uniqueKey="id"
            subKey="children"
            hideSearch
            selectText={this.state.selectCountryKey}
            readOnlyHeadings={true}
            expandDropDowns
            showDropDowns={false}
            modalWithTouchable
            confirmText={strings.close}
            searchPlaceholderText={Strings.search}
            onSelectedItemsChange={(selectedItems) => {
              // this.setState({ countries: selectedItems });
            }}
            onSelectedItemObjectsChange={(selectedItems) => {
              console.log('ITEM2   ', selectedItems[0].name);
              this.setState({ selectCountryKey: selectedItems[0].name });
            }}
          />
        </View>
        {selectCountryKey.length == 0 && (
          <Text
            style={{
              color: 'red',
              fontSize: responsiveFontSize(6),
              alignSelf: isRTL ? 'flex-start' : 'flex-end',
            }}>
            {' '}
            {Strings.require}
          </Text>
        )}
      </Animatable.View>
    );
  };

  content() {
    const { navigator, isRTL } = this.props;
    const { currentPage } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        {/*<AppHeader back navigator={navigator} title={titles[currentPage]} />*/}
        <View style={{ flex: 1 }}>
          {/*<Swiper
                            style={{flex:1}}
                            loop={false}
                            autoplay={false}
                            scrollEnabled={false}
                            showsButtons={false}
                            showsPagination={false}
                            ref={(s) => this.swiper = s}
                            index={this.state.currentPage}                            
                        >*/}

          {currentPage == 0 && this.renderInsertPhonePage()}
          {/*currentPage == 1 && this.renderVerifyCodePage()*/}
          {currentPage == 1 && this.renderInsertPasswordlPage()}
        </View>

        {this.state.loading && <LoadingDialogOverlay title={allStrings.wait} />}
      </View>
    );
  }

  render() {
    const { currentPage } = this.state;
    return (
      /*<ReactNativeParallaxHeader
        scrollViewProps={{ showsVerticalScrollIndicator: false }}
        headerMinHeight={responsiveHeight(10)}
        headerMaxHeight={responsiveHeight(35)}
        extraScrollHeight={20}
        navbarColor={colors.darkBlue}
        backgroundImage={require('../assets/imgs/header.png')}
        backgroundImageScale={1.2}
        renderNavBar={() => (
          <CollaspeAppHeader back title={titles[currentPage]} />
        )}
        renderContent={() => this.content()}
        containerStyle={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        innerContainerStyle={{ flex: 1 }}
      />*/


      <ParallaxScrollView
        backgroundColor={colors.darkBlue}
        contentBackgroundColor="white"
        renderFixedHeader={() => <CollaspeAppHeader back title={titles[currentPage]} />}
        parallaxHeaderHeight={responsiveHeight(35)}
        renderBackground={() => (
          <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

        )}
      >
        {this.content()}
      </ParallaxScrollView>
    );
  }
}

const mapToStateProps = (state) => ({
  isRTL: state.lang.RTL,
  barColor: state.lang.color,
  currentUser: state.auth.currentUser,
});

export default connect(mapToStateProps)(ForgetPassword);
