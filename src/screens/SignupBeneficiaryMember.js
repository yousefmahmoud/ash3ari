import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Modal, StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button, Radio, Container } from 'native-base';
import { login, setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import AnimateLoadingButton from 'react-native-animate-loading-button';
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader';
import { removeItem } from '../actions/MenuActions';
import StepIndicator from 'react-native-step-indicator';
import Checkbox from 'react-native-custom-checkbox';
import RadioButton from 'radio-button-react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import CodeInput from 'react-native-confirmation-code-input';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig'
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty'
import InputValidations from '../common/InputValidations'
import HTML from 'react-native-render-html';
import AsyncStorage from '@react-native-community/async-storage'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import AppSuccessDialog from '../common/AppSuccessDialog'



class SignupBeneficiaryMember extends Component {

    countryKeys = [
        // this is the parent or 'item'
        {
            name: strings.countryKey,
            id: 0,
            // these are the children or 'sub items'
            children: [
                {
                    name: '966',
                    id: 966,
                },
                {
                    name: '971',
                    id: 971,
                },
                {
                    name: '965',
                    id: 965,
                },
                {
                    name: '973',
                    id: 973,
                },
                {
                    name: '968',
                    id: 968,
                },
            ],
        },
    ];


    state = {
        currentPage: this.props.data ? this.props.data.isVerify ? 2 : 0 : 0,
        currentUserData: this.props.data ? this.props.data.userData : null,
        name: ' ',
        selectCountryID: ' ',
        selectCountryKey: '966',
        countries: [],
        selectCountry: null,
        cities: [],
        selectCity: null,
        phone: ' ',
        email: ' ',
        image: null,
        password: ' ',
        hidePassword: true,
        confirmPassword: ' ',
        hideConfirmPassword: true,
        showModal: false,
        code: ' ',
        membershipOfExcellence: 1,
        conditions: '',
        loading: false,
        availabilityEmail: true,
        availabilityPhone: true,
        verifySuccess: false,
        passwordMinLength: false

    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getCountries()
        this.getConditionsAndUsages()

    }

    getConditionsAndUsages = () => {
        axios.get(`${BASE_END_POINT}terms_user`)
            .then(response => {
                this.setState({ conditions: response.data.data })
            })
    }

    getCountries = () => {
        this.setState({ loginLoading: true })
        axios.get(`${BASE_END_POINT}locations/countries`)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {

                    var newCountries = [{
                        name: strings.countries,
                        id: 0,
                        children: response.data.data
                    }]
                    this.setState({ countries: newCountries })
                    //this.getCities(response.data.data[0].id)
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }

    getCities = (countryId) => {

        axios.get(`${BASE_END_POINT}locations/cities/${countryId}`)
            .then(response => {
                this.setState({ loginLoading: false })
                const res = response.data
                if ('data' in res) {


                    var newCities = [{
                        name: strings.countries,
                        id: 0,
                        children: response.data.data
                    }]
                    this.setState({ cities: newCities })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loginLoading: false })
            })
    }


    checkEmail = (email) => {
        var data = new FormData()
        data.append('email', email)

        axios.post(`${BASE_END_POINT}check_email`, data)
            .then(response => {

                const res = response.data

                if (response.data.status == true) {
                    this.setState({ availabilityEmail: true })
                } else {

                    this.setState({ availabilityEmail: false })
                }
            })
            .catch(error => {
                // RNToasty.Error({ title: 'fdfdf' })

            })
    }

    checkPhone = (phone) => {
        
        const {  selectCountryKey } = this.state
        var data = new FormData()
        data.append('phone', phone)
        data.append('code', selectCountryKey)

        axios.post(`${BASE_END_POINT}check_phone`, data)
            .then(response => {

                const res = response.data

                if (response.data.status == true) {
                    this.setState({ availabilityPhone: true })
                } else {

                    this.setState({ availabilityPhone: false })
                }
            })
            .catch(error => {
                // RNToasty.Error({ title: 'fdfdf' })

            })
    }

    signup = () => {
        const { name, selectCity, selectCountry, selectCountryKey, phone, email, password } = this.state
        this.setState({ loading: true })
        var data = new FormData()
        data.append('name', name)
        data.append('email', email)
        data.append('code', selectCountryKey)
        data.append('phone', phone)
        data.append('password', password)
        data.append('city_id', selectCity.id)

        axios.post(`${BASE_END_POINT}auth/register/client`, data)
            .then(response => {
                this.setState({ loading: false })
                const res = response.data

                if ('data' in res) {

                    // AsyncStorage.setItem('USER', JSON.stringify(res))
                    //AsyncStorage.setItem('VERIFY', 'yes')
                    //this.props.setUser(res)
                    this.setState({ currentPage: 2, currentUserData: res, })
                    //RNToasty.Success({ title: Strings.addUserSuccessfuly })
                } else {

                    RNToasty.Error({ title: res.msg })
                }
            })
            .catch(error => {

                this.setState({ loading: false })
            })
    }

    verify = () => {
        const { currentUser } = this.props
        const { code, currentUserData } = this.state
//console.log('Current' , currentUserData.data.phone.substring(0,3).toString())

        this.setState({ loading: true })
        var data = new FormData()
        data.append('phone', currentUserData.data.phone.split(currentUserData.data.phone.substring(0,3).toString())[1])
        data.append('verification_code', code)
        data.append('code',currentUserData.data.phone.substring(0,3).toString())

        axios.post(`${BASE_END_POINT}auth/activate`, data, {
            headers: {
                Authorization: `Bearer ${currentUserData.data.token}`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                console.log('response',response)
                const res = response.data
                if ('msg' in res) {
                    RNToasty.Error({ title: res.msg })

                } else {
                    this.props.setUser(res)
                    this.setState({verifySuccess:true})
                    resetTo('Home')
                    RNToasty.Success({ title: Strings.welcome })
                    AsyncStorage.setItem('USER', JSON.stringify(response.data))
                    this.props.setUser(response.data)

                    //AsyncStorage.removeItem('VERIFY')
                }

            })
            .catch(error => {

                this.setState({ loading: false })
            })
    }

    resendCode = () => {
        const { currentUser } = this.props
        var data = new FormData()
        data.append('phone', currentUser.data.phone)
        axios.post(`${BASE_END_POINT}auth/resend_code`, data, {
            headers: {
                "Content-Type": `multipart/form-data`
            }
        })
            .then(response => {
                this.setState({ loading: false })
                const res = response.data
                RNToasty.Success({ title: Strings.resenCodeDone })

            })
            .catch(error => {

            })
    }


    nameInput = () => {
        const { name } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(10), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(2), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='user-alt' type='FontAwesome5' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val, }) }}
                        placeholder={Strings.name}
                        placeholderTextColor={colors.lightGray}
                        value={name.replace(/\s/g, '').length ? name : null} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />

                </View>
                {name.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }


    countryInput = () => {
        const { selectCountry, selectCountryID } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ backgroundColor: colors.darkBlue, marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(2), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center' }}>
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(75), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'white', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { minHeight: responsiveHeight(50), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(18), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.countries}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountry ? this.state.selectCountry.name : Strings.selectCountry}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        hideSearch
                        modalWithTouchable
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }

                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountry: selectedItems[0], selectCountryID: selectedItems[0].id });
                            this.getCities(selectedItems[0].id)
                        }
                        }
                    />
                </View>
                {selectCountry == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    cityInput = () => {
        const { selectCity, selectCountryID } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ backgroundColor: colors.darkBlue, marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(2), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8), justifyContent: 'center', alignItems: 'center' }}>
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(75), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'white', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' },
                            subItemText: { textAlign: isRTL ? 'right' : 'left', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.state.cities}
                        alwaysShowSelectText
                        single
                        noItemsComponent={<Text style={{textAlign:'center', marginTop:moderateScale(20), fontFamily:isRTL?arrabicFont : englishFont, fontSize:responsiveFontSize(7)}}>{strings.mustChooseCountryFirst}</Text>}
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCity ? this.state.selectCity.name : Strings.selectCity}
                        readOnlyHeadings={true}
                        hideSearch
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }

                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCity: selectedItems[0] });
                        }
                        }
                    />
                </View>
                {selectCity == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    countryKeyInput = () => {
        const { countryKey, selectCountryKey } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='telephone-accessible' type='Foundation' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <SectionedMultiSelect
                        styles={{
                            selectToggle: { width: responsiveWidth(78), height: responsiveHeight(6), borderRadius: moderateScale(3), alignItems: 'center', justifyContent: 'center', color: 'white' },
                            selectToggleText: { fontSize: responsiveFontSize(6), color: 'gray', marginTop: moderateScale(6), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: isRTL ? 'right' : 'left' },
                            subItemText: { textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            itemText: { fontSize: responsiveFontSize(10), textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont },
                            container: { height: responsiveHeight(45), position: 'absolute', width: responsiveWidth(80), top: responsiveHeight(22), alignSelf: 'center' },
                            button: { backgroundColor: colors.blueTabs }
                        }}
                        items={this.countryKeys}
                        alwaysShowSelectText
                        single
                        uniqueKey="id"
                        subKey="children"
                        selectText={this.state.selectCountryKey}
                        readOnlyHeadings={true}
                        expandDropDowns
                        showDropDowns={false}
                        modalWithTouchable
                        hideSearch
                        confirmText={strings.close}
                        searchPlaceholderText={Strings.search}
                        onSelectedItemsChange={(selectedItems) => {
                            // this.setState({ countries: selectedItems });
                        }
                        }
                        onSelectedItemObjectsChange={(selectedItems) => {
                            //console.log("ITEM2   ", selectedItems[0].name)
                            this.setState({ selectCountryKey: selectedItems[0].name });


                        }
                        }
                    />
                </View>
                {selectCountryKey.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    emailInput = () => {
        const { email, availabilityEmail } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='email' type='Zocial' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { [this.setState({ email: val }), this.checkEmail(val)] }}
                        placeholder={Strings.email}
                        placeholderTextColor={colors.lightGray}
                        keyboardType={'email-address'}
                        value={email.replace(/\s/g, '').length ? email : null} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {email.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {email.length > 0 && availabilityEmail == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.emailNotAvailable}</Text>

                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone, availabilityPhone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='phone' type='FontAwesome' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        onChangeText={(val) => { [this.setState({ phone: val }), this.checkPhone(val)] }}
                        placeholder={Strings.phone}
                        placeholderTextColor={colors.lightGray}
                        keyboardType={'phone-pad'}
                        value={phone.replace(/\s/g, '').length ? phone : null} dataDetectorTypes={'phoneNumber'} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(78), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {phone.length > 0 && availabilityPhone == false &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.phoneNotAvailable}</Text>

                }
            </Animatable.View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword,passwordMinLength } = this.state
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='md-lock' type='Ionicons' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        secureTextEntry={hidePassword}
                        onChangeText={(val) => { this.setState({ password: val, passwordMinLength: false  }) }}
                        placeholder={Strings.enterYourPassword}
                        placeholderTextColor={colors.lightGray}
                        value={password.replace(/\s/g, '').length ? password : null} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(72), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hidePassword: !hidePassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hidePassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {password.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
                {passwordMinLength &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.atLeast6Char}</Text>
                }
            </Animatable.View>
        )
    }


    /*confiemPasswordInput = () => {
        const { isRTL } = this.props
        const { confirmPassword, hideConfirmPassword, confirmPasswordUdpt } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ marginTop: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center' }}>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, borderColor: colors.darkBlue, height: responsiveHeight(8) }}>
                    <View style={{ marginHorizontal: moderateScale(5), }}>
                        <Icon name='md-lock' type='Ionicons' style={{ color: colors.mediumBlue, fontSize: responsiveFontSize(8) }} />
                    </View>
                    <TextInput
                        secureTextEntry={hideConfirmPassword}
                        onChangeText={(val) => { this.setState({ confirmPassword: val, confirmPasswordUdpt: val }) }}
                        placeholder={Strings.confirmPassword} value={confirmPassword} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(72), marginHorizontal: moderateScale(0), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                    <TouchableOpacity

                        onPress={() => { this.setState({ hideConfirmPassword: !hideConfirmPassword }) }}
                    >
                        <Icon style={{ fontSize: responsiveFontSize(6), color: '#D6D6D6' }} name={hideConfirmPassword ? 'eye-off' : 'eye'} type='Feather' />
                    </TouchableOpacity>
                </View>
                {confirmPasswordUdpt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }*/

    imageInput = () => {
        const { isRTL } = this.props
        const { image } = this.state
        return (
            <View style={{ marginTop: moderateScale(10), marginHorizontal: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start' }}>

                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: 'gray' }}> {Strings.selectImage}</Text>
                <View style={{ marginTop: moderateScale(10), flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center' }}>
                    <Button
                        onPress={() => {
                            ImagePicker.openPicker({
                                width: 300,
                                height: 400,
                                cropping: true
                            }).then(image => {

                                this.setState({ image: image.path })
                            });
                        }}
                        style={{ backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center', width: responsiveWidth(30), height: responsiveHeight(20) }} >
                        <Icon name='plus' type='AntDesign' style={{ color: colors.black, fontSize: responsiveFontSize(15) }} />
                    </Button>
                    {image &&
                        <FastImage
                            source={{ uri: image }}
                            style={{ borderRadius: moderateScale(3), marginHorizontal: moderateScale(10), width: responsiveWidth(30), height: responsiveHeight(20) }}
                        />
                    }
                </View>
            </View>
        )
    }


    pagesIndicator = () => {
        return (
            <View style={{ width: responsiveWidth(80), alignSelf: 'center', marginVertical: moderateScale(10) }}>
                <StepIndicator
                    onPress={(page) => {
                        //this.setState({ currentPage: page })
                    }}
                    stepCount={3}
                    customStyles={customStyles}
                    currentPosition={this.state.currentPage}
                //labels={labels}
                />
            </View>
        )
    }

    memberSection = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginBottom: moderateScale(17), marginTop: moderateScale(15), alignSelf: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                    <Text style={{ marginHorizontal: moderateScale(1), color: colors.lightGray, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.ash3ariMember}</Text>
                    <View style={{ height: 2, width: responsiveWidth(45), backgroundColor: '#cccbcb' }} />
                </View>
                <Button onPress={() => { push('Login') }} style={{ alignSelf: 'center', marginTop: moderateScale(12), height: responsiveHeight(7), width: responsiveWidth(30), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.login2}</Text>
                </Button>
                {this.state.currentPage == 2 &&
                    <Button onPress={() => { this.resendCode() }} style={{ alignSelf: 'center', marginTop: moderateScale(6), height: responsiveHeight(7), width: responsiveWidth(50), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(1) }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.notReciveVerifyCode}</Text>
                    </Button>
                }
            </View>
        )
    }

    page1 = () => {
        const { isRTL } = this.props
        const { type, selectCity, membershipOfExcellence, name, email, phone, password, selectCountry, availabilityPhone } = this.state
        return (
            <Animatable.View style={{ width: responsiveWidth(100), }} >
                {this.nameInput()}
                {this.countryInput()}
                {this.cityInput()}
                {this.countryKeyInput()}

                {this.phoneInput()}
                {this.emailInput()}
                {this.passwordInput()}
                {/*this.confiemPasswordInput()*/}

                {/*<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginHorizontal: moderateScale(7), marginTop: moderateScale(10), alignSelf: isRTL ? 'flex-end' : 'flex-start', }} >
                    <RadioButton
                        innerCircleColor={colors.orange}
                        currentValue={this.state.membershipOfExcellence}
                        value={0}
                        onPress={(value) => { membershipOfExcellence == 0 ? this.setState({ membershipOfExcellence: 1 }) : this.setState({ membershipOfExcellence: 0 }) }} >

                    </RadioButton>
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2), marginTop: moderateScale(1) }}>{Strings.membershipOfExcellence}</Text>

                </View>
                <View style={{ marginHorizontal: moderateScale(7), marginTop: moderateScale(2), alignSelf: isRTL ? 'flex-end' : 'flex-start', }} >
                    <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, marginHorizontal: moderateScale(2), marginTop: moderateScale(1), fontSize: responsiveFontSize(5), color: 'gray', paddingHorizontal: responsiveWidth(6) }}>{Strings.membershipOfExcellenceDetails}</Text>
                </View>*/}

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginVertical: moderateScale(15), width: responsiveWidth(85), justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 0 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(1), backgroundColor: colors.grayButton }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {
                        if (!name.replace(/\s/g, '').length) {
                            this.setState({ name: '' })
                        }
                        if (!email.replace(/\s/g, '').length) {
                            this.setState({ email: '' })
                        }
                        if (InputValidations.validateEmail(email) == false) {
                            RNToasty.Error({ title: Strings.emailNotValid })
                        }

                        if (!phone.replace(/\s/g, '').length) {
                            this.setState({ phone: '' })
                        }
                        if (!password.replace(/\s/g, '').length) {
                            this.setState({ password: '' })
                        }
                        if (password.replace(/\s/g, '').length < 6) { this.setState({ passwordMinLength: true }) }
                        if (!selectCountry) {
                            this.setState({ selectCountry: false })
                        }
                        if (!selectCity) {
                            this.setState({ selectCity: false })
                        }
                        if (selectCity && selectCountry && name.replace(/\s/g, '').length && email.replace(/\s/g, '').length && InputValidations.validateEmail(email) && phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && password.replace(/\s/g, '').length >= 6 && availabilityPhone == true) {
                            this.setState({ currentPage: 1 })
                        }

                    }} style={{ padding: 0, marginHorizontal: moderateScale(10), marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(1), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>

                {/*this.memberSection()*/}

            </Animatable.View>
        )
    }

    page2 = () => {
        const { isRTL } = this.props
        const { showModal, loading } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ width: responsiveWidth(100), }} >

                <Button style={{ marginTop: moderateScale(6), borderColor: colors.darkBlue, borderWidth: 0.5, alignSelf: 'center', width: responsiveWidth(70), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.termsAndConditios} </Text>
                </Button>

                <Button onPress={() => {
                    this.setState({ showModal: true })
                }} style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(10), alignSelf: 'center', width: responsiveWidth(70), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.view} </Text>
                    <Icon name='search1' type='AntDesign' style={{ color: colors.white, fontSize: responsiveFontSize(8) }} />
                </Button>

                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop: moderateScale(6), width: responsiveWidth(85), justifyContent: 'space-between', alignSelf: 'center' }}>

                    <Button onPress={() => {
                        this.setState({ currentPage: 0 })
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(1), backgroundColor: colors.grayButton }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.previous}</Text>
                    </Button>

                    <Button onPress={() => {
                        this.signup()
                    }} style={{ padding: 0, marginTop: moderateScale(15), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(40), height: responsiveHeight(7), borderRadius: moderateScale(1), backgroundColor: colors.darkBlue }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.next}</Text>
                    </Button>
                </View>

                {this.memberSection()}

                <Modal
                    visible={showModal}
                    animationType='slide'
                    onRequestClose={() => { this.setState({ showModal: false }) }}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ width: responsiveWidth(90), height: responsiveHeight(8), alignSelf: 'center', borderColor: colors.darkGray, borderWidth: 0.7, marginTop: moderateScale(8), alignItems: 'center', justifyContent: 'center', borderRadius: moderateScale(2) }}>
                            <Text style={{ color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}>{Strings.privacyAndPolicy}</Text>
                        </View>


                        <View style={{ padding: moderateScale(7), width: responsiveWidth(90), alignSelf: 'center', borderColor: colors.darkGray, borderWidth: 0.7, marginTop: moderateScale(5) }}>
                            <HTML html={this.state.conditions} />
                        </View>
                    </ScrollView>

                    <Button onPress={() => {
                        this.setState({ showModal: false })
                    }}
                        style={{ marginVertical: moderateScale(15), alignSelf: 'center', width: responsiveWidth(60), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(3) }} >
                        <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.close} </Text>
                    </Button>

                </Modal>

                {loading && <LoadingDialogOverlay title={Strings.wait} />}
            </Animatable.View>
        )
    }



    // <Text style={{alignSelf:'center',marginTop:moderateScale(12), color:colors.darkBlue,fontFamily:isRTL?arrabicFont:englishFont}} >{Strings.verifactionCode}</Text>

    page3 = () => {
        const { isRTL } = this.props
        const { code, loading } = this.state
        return (
            <Animatable.View animation={"slideInLeft"} duration={1000} style={{ width: responsiveWidth(100), }}>
                <View style={{ alignSelf: 'center', width: responsiveWidth(90), marginTop: moderateScale(12) }}>
                <Text style={{ alignSelf: 'center',textAlign:isRTL?'right':'left', color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont , fontSize:responsiveFontSize(5.5)}} >{Strings.kindlyVerifyYourAccount}</Text>
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont }} >{Strings.verifactionCode} *</Text>
                    <TextInput
                        placeholder={Strings.enterVerifactionCode}
                        style={{ paddingHorizontal: moderateScale(2), alignSelf: 'center', marginTop: moderateScale(5), width: responsiveWidth(88), height: responsiveHeight(8), borderRadius: moderateScale(3), borderWidth: 1, borderColor: colors.lightGray, textAlign: isRTL ? 'right' : 'left' }}
                        onChangeText={(val) => {
                            this.setState({ code: val })
                        }}
                    />
                    {code.length == 0 &&
                        <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                    }
                    <Text style={{ alignSelf: isRTL ? 'flex-end' : 'flex-start', color: 'gray', fontFamily: isRTL ? arrabicFont : englishFont, marginTop: moderateScale(3), marginHorizontal: responsiveWidth(2) }} >{Strings.verificationPhoneNumber}</Text>
                </View>


                <Button onPress={() => {
                    if (!code.replace(/\s/g, '').length) {
                        this.setState({ code: '' })
                    }

                    if (code.replace(/\s/g, '').length) {
                        this.verify()
                    }

                }}
                    style={{ marginTop: moderateScale(15), alignSelf: 'center', width: responsiveWidth(40), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.greenApp, borderRadius: moderateScale(1) }} >
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.verify} </Text>
                </Button>

                {this.memberSection()}
                {loading && <LoadingDialogOverlay title={Strings.wait} />}

            </Animatable.View>
        )
    }

    content = () => {
        const { isRTL, barColor } = this.props
        const { currentPage } = this.state
        return (
            <View>
                {this.pagesIndicator()}
                {currentPage == 0 && this.page1()}
                {currentPage == 1 && this.page2()}
                {currentPage == 2 && this.page3()}

            </View>
        )
    }

    render() {
        const { isRTL, userToken } = this.props;
        const { currentPage } = this.state
        return (
           /* <ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.signupAsBeneficiaryMember} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
      backgroundColor={colors.darkBlue}
      contentBackgroundColor="white"
      renderFixedHeader={() => <CollaspeAppHeader back title={Strings.signupAsBeneficiaryMember} />}
      parallaxHeaderHeight={responsiveHeight(35)}
      renderBackground={() => (
          <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

      )}
  >
      {this.content()}
      <AppSuccessDialog
                    open={this.state.verifySuccess}
                    message={Strings.successfullyRegistered}
                    onPress={() => this.setState({ verifySuccess: false })} />
  </ParallaxScrollView>
        );
    }
}

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: 'green',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: 'green',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: 'green',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: 'green',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: 'green',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013'
}

const mapDispatchToProps = {
    login,
    removeItem,
    setUser
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
})


export default connect(mapToStateProps, mapDispatchToProps)(SignupBeneficiaryMember);

