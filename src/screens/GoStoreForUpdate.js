import React, { Component } from 'react';
import { View, RefreshControl, Alert, FlatList, Text, TouchableOpacity, Linking } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import ListFooter from '../common/ListFooter';
import { Icon, Thumbnail, Button } from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import { enableSideMenu, pop } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import NotificationCard from '../components/NotificationCard'
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import { getUnreadNotificationsNumers } from '../actions/NotificationAction';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

class GoStoreForUpdate extends Component {

    page = 1;
    state = {
        notifications: [],
        notificationsLoad: true,
        notificationsRefresh: false,
        notifications404: null,
        pages: 0,
    }

    componentDidMount() {

        enableSideMenu(false, this.props.isRTL)
        /*NetInfo.addEventListener(state => {
            if(state.isConnected){
                this.page=1
                this.getRooms(1,false);
                this.setState({loading:true,networkError:false})
            }else{
                this.setState({loading:false,networkError:true})
            }
          }); */


    }

    componentWillUnmount() {
        this.props.removeItem()
    }

    componentDidUpdate() {
        enableSideMenu(false, this.props.isRTL)
    }





    content = () => {
        const { isRTL } = this.props

        return (
            <View style={{ flex: 1, alignItems: 'center', marginTop: moderateScale(50) }}>
                <Text style={{ color: colors.darkBlue, fontSize: responsiveFontSize(8), fontFamily: isRTL ? arrabicFont : englishFont, textAlign: 'center' }}> {Strings.versionMustBeUpdate}</Text>

                <TouchableOpacity
                    onPress={() =>
                        Platform.OS === 'android' ?
                            Linking.openURL('https://play.google.com/store/apps/details?id=com.ash3arimobileapp') :
                            Linking.openURL('https://apps.apple.com/us/app/id1523239962')}
                    style={{ backgroundColor: colors.darkBlue, width: responsiveWidth(70), height: responsiveHeight(6), borderRadius: moderateScale(2), alignItems: 'center', justifyContent: 'center', marginTop: moderateScale(40) }}>
                    <Text style={{ color: 'white', textAlign: 'center', fontFamily: isRTL ? arrabicFont : englishFont, fontSize: responsiveFontSize(7) }}>{Strings.goUpdate}</Text>
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        const { categoryName, isRTL } = this.props;
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                //renderNavBar={() => <CollaspeAppHeader back={false} title={Strings.notifications} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/


            <ParallaxScrollView
                backgroundColor={colors.darkBlue}
                contentBackgroundColor="white"
                renderFixedHeader={() => <CollaspeAppHeader back title={Strings.notifications} />}
                parallaxHeaderHeight={responsiveHeight(35)}
                renderBackground={() => (
                    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

                )}
            >
                {this.content()}
            </ParallaxScrollView>

        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
    getUnreadNotificationsNumers,
}

export default connect(mapStateToProps, mapDispatchToProps)(GoStoreForUpdate);
