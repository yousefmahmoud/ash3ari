import React,{Component} from 'react';
import {View,RefreshControl,Alert,FlatList,Text,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight ,responsiveFontSize} from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import ListFooter from '../common/ListFooter';
import {Icon,Thumbnail,Button} from 'native-base'
import ChatPeopleCard from '../components/ChatPeopleCard';
import {selectMenu,removeItem} from '../actions/MenuActions';
import AppHeader from '../common/AppHeader';
import {enableSideMenu,pop,push} from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import {arrabicFont,englishFont} from  '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import Swipeout from 'react-native-swipeout';
import CollaspeAppHeader from '../common/CollaspeAppHeader'
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import PageTitleLine from '../components/PageTitleLine'
import ParallaxScrollView from 'react-native-parallax-scroll-view';

 
class ChatPeople extends Component {

    page=1;
    state= {
        messages:[],
        messagesLoad:true,
        messagesRefresh:false,
        messages404:null, 
        pages:0,
    }


    componentDidMount(){
        enableSideMenu(true,this.props.isRTL)
        this.getMessages(1,false)
    }

    componentWillUnmount(){
        this.props.removeItem()
      }

    componentDidUpdate(){
        enableSideMenu(true,this.props.isRTL)
    } 
    
    
    getMessages = (refresh,page) => {
        if(refresh){
            this.setState({messagesRefresh:true})
        }
        axios.get(`${BASE_END_POINT}inbox?page=${page}`,{
            headers:{
                Authorization: `Bearer ${this.props.currentUser.data.token}` 
            }
        })
        .then(response=>{
         
            this.setState({
              messages:refresh?response.data.data.inbox:[...this.state.messages,...response.data.data.inbox],
              messagesLoad:false,
              messagesRefresh:false,
              pages:response.data.data.paginate.total_pages,
            })
          })
        .catch(error=>{
        
          this.setState({messages404:true,messagesLoad:false,})
        })
    }


    MessagesItem = (data) => {
        const {isRTL,currentUser} = this.props
        return(
        <TouchableOpacity onPress={()=>push('Chat',data)}> 
            <Animatable.View 
                 style={{borderBottomWidth:1,borderBottomColor:'#cccbcb', marginTop:moderateScale(0), alignSelf:'center', width:responsiveWidth(96)}}
                 animation={"flipInY"} 
                 duration={1500}>
                 
                <View style={{}}>
                    <View style={{marginTop:moderateScale(6), flexDirection:isRTL?'row-reverse':'row',alignItems:'center',width:responsiveWidth(94)}}>
                        <Thumbnail source={currentUser.data.id==data.sender.id?{uri:"https://sheari.net/storage/"+data.sender.image}:{uri:"https://sheari.net/storage/"+data.receiver.image}} />
                        <View style={{marginHorizontal:moderateScale(3)}}>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:colors.darkBlue, fontSize:responsiveFontSize(7), fontFamily:isRTL?arrabicFont:englishFont}}>{currentUser.data.id==data.sender.id?data.sender.name:data.receiver.name}</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:colors.lightGray,fontSize:responsiveFontSize(5), fontFamily:isRTL?arrabicFont:englishFont}}>{currentUser.data.id==data.sender.id?data.sender.job:data.receiver.job}</Text>
                        </View>
                    </View>
                    <View style={{width:responsiveWidth(80),alignSelf:'center',marginBottom:moderateScale(6)}}>
                        <Text style={{ color:colors.black, fontSize:responsiveFontSize(7), fontFamily:isRTL?arrabicFont:englishFont}}>{data.message}</Text>
                    </View>
                </View> 

                    
                </Animatable.View>
        </TouchableOpacity>
                
        )
    }

    content = () => {
        const {messages,messages404,messagesLoad,pages,messagesRefresh} = this.state
         return(
           <View>
            <PageTitleLine title={Strings.messageBox} iconName='mail' iconType='Entypo' />
            {
            messages404?
            <NetworError />
            :
            messagesLoad?
            <Loading />
            :
            messages.length>0?
            <FlatList 
            style={{marginBottom:moderateScale(10)}}
            data={messages}
            renderItem={({item})=>this.MessagesItem(item)}
            onEndReachedThreshold={.5}
            //ListFooterComponent={()=>notificationsLoad&&<ListFooter />}
            onEndReached={() => {     
                if(this.page <= pages){
                    this.page=this.page+1;
                    this.getMessages(false,this.page)
                  
                }  
            }}
            refreshControl={
            <RefreshControl 
            colors={["#B7ED03",colors.darkBlue]} 
            refreshing={messagesRefresh}
            onRefresh={() => {
                this.page = 1
                this.getMessages(true,1)
            }}
            />
            }
            
            />
            :
            <NoData />
            }
           
          </View>
         )
       }
 

    render(){
        const {categoryName,isRTL} = this.props;
        return(
            /*<View style={{flex:1}}>
            <ReactNativeParallaxHeader
             scrollViewProps={{showsVerticalScrollIndicator:false}}
             headerMinHeight={responsiveHeight(10)}
             headerMaxHeight={responsiveHeight(35)}
             extraScrollHeight={20}
             navbarColor={colors.darkBlue}
             backgroundImage={require('../assets/imgs/header.png')}
             backgroundImageScale={1.2}
             renderNavBar={()=><CollaspeAppHeader title={Strings.messageBox} />}
             renderContent={()=>this.content()}
             containerStyle={{flex:1}}
             contentContainerStyle={{flexGrow: 1}}
             innerContainerStyle={{flex: 1,}}
           />                     
            </View>*/


<ParallaxScrollView
backgroundColor={colors.darkBlue}
contentBackgroundColor="white"
renderFixedHeader={() => <CollaspeAppHeader title={Strings.messageBox} />}
parallaxHeaderHeight={responsiveHeight(35)}
renderBackground={() => (
    <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

)}
>
{this.content()}
</ParallaxScrollView>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    currentUser:state.auth.currentUser, 
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps,mapDispatchToProps)(ChatPeople);
