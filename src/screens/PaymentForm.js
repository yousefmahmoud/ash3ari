import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import {
    View, Animated, ScrollView, TouchableOpacity, Text, Modal, ImageBackground, TextInput, StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Thumbnail, Button } from "native-base";
import { responsiveWidth, moderateScale, responsiveFontSize, responsiveHeight } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { Field, reduxForm } from "redux-form"
import { getUser } from '../actions/AuthActions';
import { BASE_END_POINT } from '../AppConfig'
import axios from 'axios';
import { selectMenu, removeItem } from '../actions/MenuActions';
import AppHeader from '../common/AppHeader'
import strings from '../assets/strings';
import { enableSideMenu, push, pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import CollaspeAppHeader from '../common/CollaspeAppHeader';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import InputValidations from '../common/InputValidations'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FastImage from 'react-native-fast-image'



class PaymentForm extends Component {

    state = {
        name: '', nameUpdt: ' ',
        email: '', emailUpdt: ' ',
        phone: ' ',
        orderAddress: '', orderAddressUpdt: ' ',
        expectedTime: '', expectedTimeUpdt: ' ',
        estimatedBudget: '', estimatedBudgetUpdt: ' ',
        orderDescription: '', orderDescriptionUpdt: ' ',
        message: ' ',
        selectCategory: null,
        selectImportance: null,
        loading: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)

    }

    componentDidUpdate() {
        enableSideMenu(false, null)
    }


    MakeOrder = () => {
        const { name, email, importance, expectedTime, estimatedBudget, orderDescription } = this.state
        const { data } = this.props

        var day = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        var currentDate = year + '-' + month + '-' + day
        var currentTime = hours + ':' + min

        if (!name.replace(/\s/g, '').length) {
            this.setState({ name: '', nameUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '', emailUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        } else {
            if (InputValidations.validateEmail(email) == false) {
                RNToasty.Error({ title: Strings.emailNotValid })
            }
        }
        if (!orderDescription.replace(/\s/g, '').length) {
            this.setState({ orderDescription: '', orderDescriptionUpdt: '' })
            RNToasty.Error({ title: Strings.insertTheRequiredData })
        }
        if (name.replace(/\s/g, '').length && email.replace(/\s/g, '').length && orderDescription.replace(/\s/g, '').length && InputValidations.validateEmail(email) == true) {
            this.setState({ loginLoading: true })
            var dataSend = new FormData()
            dataSend.append('title', name)
            dataSend.append('details', orderDescription)
            dataSend.append('time', currentTime)
            dataSend.append('date', currentDate)
            dataSend.append('provider_id', data.id)
            dataSend.append('important', importance)
            dataSend.append('expected_time', expectedTime)
            dataSend.append('expected_money', estimatedBudget)
            dataSend.append('lng', '30')
            dataSend.append('lat', '31')

            axios.post(`${BASE_END_POINT}client/ordered/send`, dataSend, {
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + this.props.currentUser.data.token
                    //'Content-Type': 'multipart/form-data',
                },
            })
                .then(response => {
                    this.setState({ loginLoading: false })
                    const res = response.data
                    if ('data' in res) {
                        console.log('Done  ', res)
                        RNToasty.Success({ title: Strings.orderSentSuccessfuly })
                        //this.setState({ currentPage: 4 })
                    } else {
                        if (response.data.status == true) {
                            RNToasty.Success({ title: res.message })
                            // this.setState({ currentPage: 4 })
                        } else {
                            console.log('Error  ', res)
                            RNToasty.Error({ title: res.msg })
                        }
                    }
                })
                .catch(error => {
                    RNToasty.Error({ title: 'fdfdf' })
                    console.log('Error  ', error)
                    this.setState({ loginLoading: false })

                })
        }
    }

    nameInput = () => {
        const { name, nameUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.name}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ name: val, nameUpdt: val }) }}
                        placeholder={Strings.name} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {nameUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    emailInput = () => {
        const { email, emailUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.email}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ email: val, emailUpdt: val }) }}
                        placeholder={Strings.email} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {emailUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    phoneInput = () => {
        const { phone } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.phone}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        placeholder={Strings.phone} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {phone.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    orderAddressInput = () => {
        const { orderAddress, orderAddressUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderAddress}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ orderAddress: val }) }}
                        placeholder={Strings.createAnAttractivTitle} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(83), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {/*orderAddressUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>*/
                }
            </Animatable.View>
        )
    }


    expectedTimeInput = () => {
        const { expectedTime, expectedTimeUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View >
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.expectedTime}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ expectedTime: val, expectedTimeUpdt: val }) }}
                        placeholder={Strings.expectedTime} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {expectedTimeUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    estimatedBudgetInput = () => {
        const { estimatedBudget, estimatedBudgetUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(5), width: responsiveWidth(85), alignSelf: 'center' }}>
                <View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.estimatedBudget}</Text>
                </View>
                <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(85), borderBottomRadius: moderateScale(10), borderBottomWidth: 0.5, borderColor: '#cccbcb', height: responsiveHeight(8) }}>
                    <TextInput
                        onChangeText={(val) => { this.setState({ estimatedBudget: val, estimatedBudgetUpdt: val }) }}
                        placeholder={Strings.estimatedBudget} underlineColorAndroid='transparent' style={{ fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {estimatedBudgetUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    orderDescriptionInput = () => {
        const { orderDescription, orderDescriptionUpdt } = this.state
        const { isRTL } = this.props
        return (
            <Animatable.View style={{ marginTop: moderateScale(7), width: responsiveWidth(90), alignSelf: 'center' }}>
                <View>
                    <Text style={{ color: 'black', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-end' : 'flex-start', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.orderDescription} ({Strings.optional})</Text>
                </View>
                <View style={{ borderColor: colors.lightGray, flexDirection: isRTL ? 'row-reverse' : 'row', justifyContent: 'center', backgroundColor: 'white', marginTop: moderateScale(1), width: responsiveWidth(90), borderRadius: moderateScale(3), borderWidth: 0.5, height: responsiveHeight(40), marginTop: moderateScale(5) }}>

                    <TextInput
                        onChangeText={(val) => { this.setState({ orderDescription: val, orderDescriptionUpdt: val }) }}
                        multiline={true}
                        placeholder={Strings.orderDescription} underlineColorAndroid='transparent' style={{ textAlignVertical: 'top', fontSize: responsiveFontSize(6), fontFamily: isRTL ? arrabicFont : englishFont, width: responsiveWidth(85), color: 'gray', direction: isRTL ? 'rtl' : 'ltr', textAlign: isRTL ? 'right' : 'left' }} />
                </View>
                {orderDescriptionUpdt.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </Animatable.View>
        )
    }

    nextpreviousButton = () => {
        const { name, email, phone, message } = this.state
        const { currentUser, isRTL } = this.props;
        return (
            <View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: responsiveWidth(70), marginVertical: moderateScale(10) }}>
                <Button onPress={() => pop()} style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.grayButton }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.previous}</Text>
                </Button>

                <Button
                    onPress={() => { this.MakeOrder() }}
                    style={{ marginHorizontal: moderateScale(5), width: responsiveWidth(30), height: responsiveHeight(7), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: moderateScale(3), backgroundColor: colors.darkBlue }}>
                    <Text style={{ color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.next}</Text>
                </Button>
            </View>
        )
    }


    content = () => {
        const { isRTL, barColor } = this.props
        return (
            <View>
                <View style={{ borderWidth: 1, borderColor: colors.lightGray, borderRadius: moderateScale(5), width: responsiveWidth(90), alignSelf: 'center', marginTop: moderateScale(10), }}>
                    {this.nameInput()}
                    {this.emailInput()}
                    {this.phoneInput()}
                    {this.orderAddressInput()}
                    {this.categoryAndImportancePickers()}
                    {this.expectedTimeInput()}
                    {this.estimatedBudgetInput()}
                </View>
                {this.orderDescriptionInput()}
                {this.nextpreviousButton()}
            </View>
        )
    }

    render() {
        const { currentUser, isRTL } = this.props;
        const { name, email, phone, message } = this.state
        return (
            /*<ReactNativeParallaxHeader
                scrollViewProps={{ showsVerticalScrollIndicator: false }}
                headerMinHeight={responsiveHeight(10)}
                headerMaxHeight={responsiveHeight(35)}
                extraScrollHeight={20}
                navbarColor={colors.darkBlue}
                backgroundImage={require('../assets/imgs/header.png')}
                backgroundImageScale={1.2}
                renderNavBar={() => <CollaspeAppHeader back title={Strings.makeOrder} />}
                renderContent={() => this.content()}
                containerStyle={{ flex: 1 }}
                contentContainerStyle={{ flexGrow: 1 }}
                innerContainerStyle={{ flex: 1, }}
            />*/

            <ParallaxScrollView
            backgroundColor={colors.darkBlue}
            contentBackgroundColor="white"
            renderFixedHeader={() => <CollaspeAppHeader back title={Strings.thePay} />}
            parallaxHeaderHeight={responsiveHeight(35)}
            renderBackground={() => (
                <FastImage source={require('../assets/imgs/header.png')} style={{ height: responsiveHeight(35), alignItems: 'center', justifyContent: 'center' }} />

            )}
        >
            {this.content()}
        </ParallaxScrollView>
        );
    }
}


const mapDispatchToProps = {
    getUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
})

export default connect(mapToStateProps, mapDispatchToProps)(PaymentForm);