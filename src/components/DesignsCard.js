import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {push} from '../controlls/NavigationControll'



const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class DesignsCard extends Component {
  

    
    render(){
        const {isRTL,navigator,data} = this.props;
        return(
        <TouchableOpacity > 
        <Animatable.View  animation={"bounceIn"} duration={1500} style={{ alignItems:'center', marginVertical:moderateScale(2), backgroundColor:'white',borderRadius:moderateScale(3),width:responsiveWidth(40),height:responsiveHeight(20),marginTop:moderateScale(2), alignSelf:'center',justifyContent:'center' }}>          
          <Thumbnail 
          large
          source={{uri:data.image}}
          />
        
        <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(7),color:colors.black,textAlign:'center'}} >{data.name}</Text> 
         
        </Animatable.View>  
        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(DesignsCard);
