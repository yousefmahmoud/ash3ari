import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import {Icon} from 'native-base'
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import { Thumbnail } from 'native-base';
import {push} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'



class ChatPeopleCard extends Component {
    
    render(){
        const {data,isRTL} = this.props;
        return(
            <TouchableOpacity 
            onPress={()=>{
                push('Chat',data)
            }}
            style={{borderBottomColor:'#DEDEDE',borderBottomWidth:0.4, justifyContent:'center', height:responsiveHeight(12),width:responsiveWidth(100)}}>
                <View style={{alignItems:'center', marginHorizontal:moderateScale(5),flexDirection:'row'}}>
                    <Thumbnail small source={require('../assets/imgs/logo.png')} />
                    <View style={{maxWidth:responsiveWidth(80), marginHorizontal:moderateScale(3)}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont, alignSelf:'flex-start',fontSize:responsiveFontSize(7),color:'gray'}}>{data.title}</Text>             
                    </View>

                </View>
            </TouchableOpacity>      
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
}


export default connect(mapStateToProps,mapDispatchToProps)(ChatPeopleCard);
