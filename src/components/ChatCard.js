
import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text,ImageBackground} from 'react-native';
import {Icon} from 'native-base'
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import { Thumbnail } from 'native-base';
import moment from 'moment'
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
//import { SwipeItem, SwipeButtonsContainer } from 'react-native-swipe-item';
import Swipeout from 'react-native-swipeout';
import {openImage} from '../actions/ChatAction';
import {enableSideMenu,push} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as colors from '../assets/colors'


class ChatCard extends Component {

    render(){
        const {chatRow,currentUser,isRTL} = this.props;
        return(
        <Swipeout 
        close
        style={{backgroundColor:'white'}}
         >
            <View style={{flexDirection:'row', margin:moderateScale(2),justifyContent:'center',alignSelf:chatRow.sender_id==currentUser.data.id?'flex-end':'flex-start'}}>
               {chatRow.sender_id!=currentUser.data.id&& <Thumbnail small source={{uri:chatRow.receiver_image}} />}
                      
                <View style={{ justifyContent:'center', alignSelf:chatRow.sender_id==currentUser.data.id?'flex-end':'flex-start'}}>
                    <TouchableOpacity 
                    style={{maxWidth:responsiveWidth(70), flexDirection:chatRow.sender_id==currentUser.data.id?'row-reverse':'row', minHeight:responsiveHeight(7),alignItems:'center', borderRadius:moderateScale(5),alignSelf:chatRow.sender_id==currentUser.data.id?'flex-end':'flex-start',marginHorizontal:moderateScale(1),backgroundColor:chatRow.sender_id==currentUser.data.id?colors.darkBlue:'#C7BE75'}}
                    >
                        <View style={{maxWidth:responsiveWidth(60),margin:moderateScale(3),}}>
                         <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'white'}}>{chatRow.message}</Text>         
                        </View>
                    </TouchableOpacity>       
                </View>
                   
            </View>
    
        </Swipeout >
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    barColor: state.lang.color
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers,
    openImage
}


export default connect(mapStateToProps,mapDispatchToProps)(ChatCard);
