import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {push} from '../controlls/NavigationControll'



const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

//moment(birth_date).format("YYYY-MM-DD")


class AdsCard extends Component {
  

    
    render(){
        const {isRTL,navigator,data} = this.props;
        return(
        <TouchableOpacity style={{justifyContent:'center',alignItems:'center', marginVertical:moderateScale(2),borderRadius:moderateScale(5),width:responsiveWidth(90),marginHorizontal:moderateScale(5),height:responsiveHeight(9),marginTop:moderateScale(5), borderStyle:'solid', borderWidth:1,borderColor:'gray', alignSelf:'center' }}
         onPress={()=>push('ServiceProviders',data)}> 
        <Animatable.View  style={{width:responsiveWidth(70),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',}}>          
          <FastImage 
          style={{borderRadius:moderateScale(2), width:responsiveWidth(18),height:responsiveHeight(7.5),}}
          source={{uri:data.image}}
          />
        
        <Text style={{  marginHorizontal:moderateScale(10), fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(7),color:colors.black,textAlign:'center'}} >{data.name}</Text> 
         
        </Animatable.View>  
        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(AdsCard);
