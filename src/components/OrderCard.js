import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text,Modal} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';
import {getUnreadNotificationsNumers} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {push} from '../controlls/NavigationControll'


class OrderCard extends Component {
    
    state={
        show:false,
    }

    item = (key,val,isButton) => {
        const {isRTL} = this.props
        return(
            <View style={{borderBottomWidth:0,borderBottomColor:'#cccbcb', marginTop:moderateScale(0), flexDirection:isRTL?'row-reverse':'row',alignItems:'center', width:responsiveWidth(90),height:responsiveHeight(5),marginVertical:isButton?moderateScale(5):0 }} >
                <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:colors.sky,textAlign:'center',marginHorizontal:moderateScale(6)}} >{key} : </Text> 
                {isButton?
                <TouchableOpacity style={{borderWidth:2,borderRadius:moderateScale(3), borderColor:colors.lightGray,padding:moderateScale(2)}} onPress={()=>{this.setState({show:true})}}>
                    <Text style={{ fontFamily:englishFont,fontSize:responsiveFontSize(6),color:'green',textAlign:'center',marginHorizontal:moderateScale(0)}} >{val}</Text> 
                </TouchableOpacity>
                :
                <Text style={{ fontFamily:englishFont,fontSize:responsiveFontSize(6),color:'gray',textAlign:'center',marginHorizontal:moderateScale(0)}} >{val}</Text> 
                }
                </View>
        )
    }
    
    render(){
        const {isRTL,navigator,data} = this.props;
        const {show} = this.state
        return(
         <TouchableOpacity>   
        <Animatable.View  animation={"flipInY"} duration={1500} style={{marginVertical:moderateScale(2), backgroundColor:colors.white,borderRadius:moderateScale(3),width:responsiveWidth(90),shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,elevation:4,marginTop:moderateScale(5), alignSelf:'center' }}>          
          {this.item(Strings.orderNumber,"12345678")}
          {this.item(Strings.orderDate,"12/4/1997")}
          {this.item(Strings.orderType,"website design")}
          {this.item(Strings.orderStatus,"New",true)}

        {/*<Button style={{marginVertical:moderateScale(7),alignSelf:'center',borderRadius:moderateScale(3), height:responsiveHeight(7),width:responsiveWidth(40),justifyContent:'center',alignItems:'center',backgroundColor:colors.darkBlue}} onPress={()=>{this.setState({show:true})}}>
             <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:'white',textAlign:'center',marginHorizontal:moderateScale(6)}} >{Strings.details}</Text> 
        </Button>*/}

        </Animatable.View> 

        <Modal
        visible={show}
        animationType='slide'
        onRequestClose={()=>{this.setState({show:false})}}
        >

            <Button transparent style={{margin:moderateScale(7),alignSelf:isRTL?'flex-start':'flex-end', justifyContent:'center',alignItems:'center'}} onPress={()=>{this.setState({show:false})}}>
               <Icon name='close' type='AntDesign' style={{color:colors.sky}} />
            </Button>

          <View style={{alignSelf:'center'}}>
          {this.item(Strings.orderNumber,"12345678")}
          {this.item(Strings.orderDate,"12/4/1997")}
          {this.item(Strings.orderType,"website design")}
          {this.item(Strings.orderStatus,"New")}
          {this.item(Strings.orderNumber,"12345678")}
          {this.item(Strings.orderDate,"12/4/1997")}
          {this.item(Strings.orderType,"website design")}
          {this.item(Strings.orderStatus,"New")}
          </View>

          <View style={{flexDirection:isRTL?'row-reverse':'row', width:responsiveWidth(100),position:'absolute',bottom:0,left:0,right:0}}>
            <Button style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:colors.darkBlue}} onPress={()=>{this.setState({show:false})}}>
                <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:'white',textAlign:'center',marginHorizontal:moderateScale(6)}} >{Strings.agree}</Text> 
            </Button>
            <Button style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:colors.sky}} onPress={()=>{this.setState({show:false})}}>
                <Text style={{ fontFamily:isRTL?arrabicFont:englishFont,fontSize:responsiveFontSize(6),color:'white',textAlign:'center',marginHorizontal:moderateScale(6)}} >{Strings.refuse}</Text> 
            </Button>
          </View>

        </Modal>

        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsNumers
}


export default connect(mapStateToProps,mapDispatchToProps)(OrderCard);
