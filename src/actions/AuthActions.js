//import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import {
   CURRENT_USER, IS_VERIFY,
  
} from './types';
import { LOGIN, BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';
import { resetTo } from '../controlls/NavigationControll'


export function setUser(user) {
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}


