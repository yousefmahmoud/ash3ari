import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT } from '../AppConfig';
import {
    GET_ADS_CATEGORIES
} from './types';
import { RNToasty } from 'react-native-toasty';



export function getAdsCategories() {
   
    return dispatch => {
        let uri = `${BASE_END_POINT}get-ads-categories`

        dispatch({ type: GET_ADS_CATEGORIES });

        axios.get(uri, {
            headers: {
                //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0OSIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQyNzI1MTIyNTY1LCJleHAiOjE1NDI3MjU3MjczNjV9.iYKdxuK0fpf0ZcQQV77rjhZ4VCZ2O0TaRsisSh5RoAA
                //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyMCIsImlzcyI6ImJvb2RyQ2FyIiwiaWF0IjoxNTQwNjUxMjk1NTUzLCJleHAiOjE1NDA2NTE5MDAzNTN9.vf2XkQfewGwBOyU4d-Yo7K6nu2xoVmHUDh-Xl5_DCgc
                'Content-Type': 'application/json',
                //token
                //'Authorization': `Bearer ${token}`
                //'Postman-Token': '91976ad4-f634-45bd-ba71-9b5efcd5608b'
            },
        })
            .then(response => {
                
                
                if (page == 1) {
                    if (response.data.data.length == 0) {
                        RNToasty.Info({ title: Strings.notNotificatios })
                    }
                }
                dispatch({ type: GET_ADS_CATEGORIES, payload: response.data.data })
            }).catch(error => {
              
                if (!error.response) {
                    dispatch({ type: GET_ADS_CATEGORIES, payload: Strigs.noConnection })
                }
            })
    }
}