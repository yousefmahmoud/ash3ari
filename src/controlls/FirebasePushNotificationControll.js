import React,{Component} from 'react';
import {Alert} from 'react-native'
import firebase,{Notification } from 'react-native-firebase';
import { push } from './NavigationControll';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import {BASE_END_POINT} from '../AppConfig'

export const checkFirbaseNotificationPermision = ()=>{
    firebase.messaging().hasPermission()
    .then(enabled => {
        if (enabled) {
        } else {
        firebase.messaging().requestPermission()
        } 
    });  
}

export const getFirebaseNotificationToken = (userToken)=>{
    firebase.messaging().getToken().then(token => {
       
        sendFirebasTokenToServer(token.toString(),userToken)
      }).catch(error=>{
              
      });

}

const sendFirebasTokenToServer = (firebaseToken,userToken) =>{
    //write action with api  
    var data = new FormData()
    data.append('fcm_token_android',firebaseToken)
    data.append('fcm_token_ios',firebaseToken)
    axios.post(`${BASE_END_POINT}user/update_fcm_token`,data,{
        headers:{
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${userToken}`
        }
    }
    )
    .then((res)=>{
        //Alert.alert("Done")
    })
    .catch((error)=>{
        //Alert.alert('ERROR')
    })
}

const firebaseNotificatioComponent = (title, body) => {
 
    const notification = new firebase.notifications.Notification()
     .setNotificationId(new Date().valueOf().toString()) 
     .setTitle(title)
     .setBody(body)
     //.setBadge(1)
     .android.setPriority(firebase.notifications.Android.Priority.High)
     .android.setChannelId("reminder") // should be the same when creating channel for Android
     .android.setAutoCancel(true);
     //firebase.notifications().setBadge(2)
    firebase.notifications().displayNotification(notification) 
 }
 
 export const showFirebaseNotifcation = (notiCount) => {
     firebase.notifications().onNotification((notification) => {
         //console.log("showFirebaseNotifcation")
         //console.log(notification)
        
         //console.log("NOTI INFO   "+notification._title,notification._body)
         firebaseNotificatioComponent(notification._title,notification._body)
     });
    
 }

 const doAction = async () => {
    const userJSON = await AsyncStorage.getItem('USER');
    if (userJSON) {
        push('Notifications')
    } else {
        push('Login')
    }
}

export const clickOnFirebaseNotification = ()=>{
    firebase.notifications().onNotificationOpened((notificationOpen) => {
        firebase.notifications().removeDeliveredNotification(notificationOpen.notification._notificationId)
        doAction()
    });
}


