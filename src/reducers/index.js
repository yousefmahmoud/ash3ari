import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import MenuReducer from "./MenuReducer";
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import NotificationsReducer from './NotificationsReducer';
import ChatReducer from './ChatReducer';
import CategoriesReducer from './CategoriesReducer'
import OrdersReducer from './OrdersReducer'


export default combineReducers({
    form: formReducer,
    menu: MenuReducer,
    auth: AuthReducer,
    lang: LanguageReducer,
    noti: NotificationsReducer,
    chat: ChatReducer,
    orderCount : OrdersReducer
    //Categories: CategoriesReducer
});