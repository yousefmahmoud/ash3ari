import {
    ORDERS_COUNT
} from '../actions/types';



const initState = {
    loading:false,
   
    pages:null,
    refresh: false,
    ordersCount: 0,
}

const OrdersReducer = (state=initState, action) => {
    switch(action.type){
        case ORDERS_COUNT:
            return { ...state,ordersCount:action.payload };   
            
            default: 
        return state; 
    }
}

export default OrdersReducer;