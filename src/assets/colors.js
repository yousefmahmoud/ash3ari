export const white = 'white';
export const black = '#373737';
export const darkBlue = '#16476A'
export const sky = '#55B1C2'
export const lightBlue = '#2196F3'
export const blueTabs = '#0b97bd'
export const lightGray = '#cccbcb'
export const green = 'green'
export const greenApp = '#80ad30'
export const darkGray = '#6b6b6b'
export const grayButton = '#b2b2b2'
export const mediumBlue = '#258fd3'
export const orange = "#fe9f00"

